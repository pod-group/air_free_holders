EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x12 J1
U 1 1 5E46D145
P 1700 2250
F 0 "J1" H 1780 2242 50  0000 L CNN
F 1 "Conn_01x12" H 1780 2151 50  0000 L CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x12_P2.00mm_Vertical" H 1700 2250 50  0001 C CNN
F 3 "~" H 1700 2250 50  0001 C CNN
	1    1700 2250
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J2
U 1 1 5E4A3A00
P 1950 5000
F 0 "J2" H 2030 5042 50  0000 L CNN
F 1 "Conn_01x01" H 2030 4951 50  0000 L CNN
F 2 "myParts:pogoPin" H 1950 5000 50  0001 C CNN
F 3 "~" H 1950 5000 50  0001 C CNN
	1    1950 5000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J3
U 1 1 5E4A3F20
P 1950 5250
F 0 "J3" H 2030 5292 50  0000 L CNN
F 1 "Conn_01x01" H 2030 5201 50  0000 L CNN
F 2 "myParts:pogoPin" H 1950 5250 50  0001 C CNN
F 3 "~" H 1950 5250 50  0001 C CNN
	1    1950 5250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J4
U 1 1 5E4A426D
P 1950 5500
F 0 "J4" H 2030 5542 50  0000 L CNN
F 1 "Conn_01x01" H 2030 5451 50  0000 L CNN
F 2 "myParts:pogoPin" H 1950 5500 50  0001 C CNN
F 3 "~" H 1950 5500 50  0001 C CNN
	1    1950 5500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J5
U 1 1 5E4A43A8
P 1950 5750
F 0 "J5" H 2030 5792 50  0000 L CNN
F 1 "Conn_01x01" H 2030 5701 50  0000 L CNN
F 2 "myParts:pogoPin" H 1950 5750 50  0001 C CNN
F 3 "~" H 1950 5750 50  0001 C CNN
	1    1950 5750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J6
U 1 1 5E4A4571
P 1950 6000
F 0 "J6" H 2030 6042 50  0000 L CNN
F 1 "Conn_01x01" H 2030 5951 50  0000 L CNN
F 2 "myParts:pogoPin" H 1950 6000 50  0001 C CNN
F 3 "~" H 1950 6000 50  0001 C CNN
	1    1950 6000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J7
U 1 1 5E4A5B07
P 1950 6250
F 0 "J7" H 2030 6292 50  0000 L CNN
F 1 "Conn_01x01" H 2030 6201 50  0000 L CNN
F 2 "myParts:pogoPin" H 1950 6250 50  0001 C CNN
F 3 "~" H 1950 6250 50  0001 C CNN
	1    1950 6250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J8
U 1 1 5E4A5B0D
P 1950 6500
F 0 "J8" H 2030 6542 50  0000 L CNN
F 1 "Conn_01x01" H 2030 6451 50  0000 L CNN
F 2 "myParts:pogoPin" H 1950 6500 50  0001 C CNN
F 3 "~" H 1950 6500 50  0001 C CNN
	1    1950 6500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J9
U 1 1 5E4A5B13
P 1950 6750
F 0 "J9" H 2030 6792 50  0000 L CNN
F 1 "Conn_01x01" H 2030 6701 50  0000 L CNN
F 2 "myParts:pogoPin" H 1950 6750 50  0001 C CNN
F 3 "~" H 1950 6750 50  0001 C CNN
	1    1950 6750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J10
U 1 1 5E4A5B19
P 1950 7000
F 0 "J10" H 2030 7042 50  0000 L CNN
F 1 "Conn_01x01" H 2030 6951 50  0000 L CNN
F 2 "myParts:pogoPin" H 1950 7000 50  0001 C CNN
F 3 "~" H 1950 7000 50  0001 C CNN
	1    1950 7000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J11
U 1 1 5E4A5B1F
P 1950 7250
F 0 "J11" H 2030 7292 50  0000 L CNN
F 1 "Conn_01x01" H 2030 7201 50  0000 L CNN
F 2 "myParts:pogoPin" H 1950 7250 50  0001 C CNN
F 3 "~" H 1950 7250 50  0001 C CNN
	1    1950 7250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J13
U 1 1 5E4A820F
P 3400 5000
F 0 "J13" H 3480 5042 50  0000 L CNN
F 1 "Conn_01x01" H 3480 4951 50  0000 L CNN
F 2 "myParts:pogoPin" H 3400 5000 50  0001 C CNN
F 3 "~" H 3400 5000 50  0001 C CNN
	1    3400 5000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J14
U 1 1 5E4A8215
P 3400 5250
F 0 "J14" H 3480 5292 50  0000 L CNN
F 1 "Conn_01x01" H 3480 5201 50  0000 L CNN
F 2 "myParts:pogoPin" H 3400 5250 50  0001 C CNN
F 3 "~" H 3400 5250 50  0001 C CNN
	1    3400 5250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J15
U 1 1 5E4A821B
P 3400 5500
F 0 "J15" H 3480 5542 50  0000 L CNN
F 1 "Conn_01x01" H 3480 5451 50  0000 L CNN
F 2 "myParts:pogoPin" H 3400 5500 50  0001 C CNN
F 3 "~" H 3400 5500 50  0001 C CNN
	1    3400 5500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J16
U 1 1 5E4A8221
P 3400 5750
F 0 "J16" H 3480 5792 50  0000 L CNN
F 1 "Conn_01x01" H 3480 5701 50  0000 L CNN
F 2 "myParts:pogoPin" H 3400 5750 50  0001 C CNN
F 3 "~" H 3400 5750 50  0001 C CNN
	1    3400 5750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J17
U 1 1 5E4A8227
P 3400 6000
F 0 "J17" H 3480 6042 50  0000 L CNN
F 1 "Conn_01x01" H 3480 5951 50  0000 L CNN
F 2 "myParts:pogoPin" H 3400 6000 50  0001 C CNN
F 3 "~" H 3400 6000 50  0001 C CNN
	1    3400 6000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J18
U 1 1 5E4A822D
P 3400 6250
F 0 "J18" H 3480 6292 50  0000 L CNN
F 1 "Conn_01x01" H 3480 6201 50  0000 L CNN
F 2 "myParts:pogoPin" H 3400 6250 50  0001 C CNN
F 3 "~" H 3400 6250 50  0001 C CNN
	1    3400 6250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J19
U 1 1 5E4A8233
P 3400 6500
F 0 "J19" H 3480 6542 50  0000 L CNN
F 1 "Conn_01x01" H 3480 6451 50  0000 L CNN
F 2 "myParts:pogoPin" H 3400 6500 50  0001 C CNN
F 3 "~" H 3400 6500 50  0001 C CNN
	1    3400 6500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J20
U 1 1 5E4A8239
P 3400 6750
F 0 "J20" H 3480 6792 50  0000 L CNN
F 1 "Conn_01x01" H 3480 6701 50  0000 L CNN
F 2 "myParts:pogoPin" H 3400 6750 50  0001 C CNN
F 3 "~" H 3400 6750 50  0001 C CNN
	1    3400 6750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J21
U 1 1 5E4A823F
P 3400 7000
F 0 "J21" H 3480 7042 50  0000 L CNN
F 1 "Conn_01x01" H 3480 6951 50  0000 L CNN
F 2 "myParts:pogoPin" H 3400 7000 50  0001 C CNN
F 3 "~" H 3400 7000 50  0001 C CNN
	1    3400 7000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J22
U 1 1 5E4A8245
P 3400 7250
F 0 "J22" H 3480 7292 50  0000 L CNN
F 1 "Conn_01x01" H 3480 7201 50  0000 L CNN
F 2 "myParts:pogoPin" H 3400 7250 50  0001 C CNN
F 3 "~" H 3400 7250 50  0001 C CNN
	1    3400 7250
	1    0    0    -1  
$EndComp
Text Notes 5300 1450 0    50   ~ 0
Pixels Even\nCircle Fiducial
Text Notes 1550 3050 0    50   ~ 0
Pixels Odd\nCross Fiducial
Text GLabel 3200 5000 0    50   Input ~ 0
TCO_4A
Text GLabel 3200 5250 0    50   Input ~ 0
TCO_4B
Text GLabel 3200 5500 0    50   Input ~ 0
Pix_6B
Text GLabel 3200 7250 0    50   Input ~ 0
TCO_2A
Text GLabel 3200 5750 0    50   Input ~ 0
Pix_6A
Text GLabel 3200 6000 0    50   Input ~ 0
Pix_4B
Text GLabel 3200 6250 0    50   Input ~ 0
Pix_4A
Text GLabel 3200 6500 0    50   Input ~ 0
Pix_2B
Text GLabel 3200 6750 0    50   Input ~ 0
Pix_2A
Text GLabel 3200 7000 0    50   Input ~ 0
TCO_2B
Text GLabel 1750 5000 0    50   Input ~ 0
TCO_3A
Text GLabel 1750 5250 0    50   Input ~ 0
TCO_3B
Text GLabel 1750 5500 0    50   Input ~ 0
Pix_5B
Text GLabel 1750 5750 0    50   Input ~ 0
Pix_5A
Text GLabel 1750 6000 0    50   Input ~ 0
Pix_3B
Text GLabel 1750 6250 0    50   Input ~ 0
Pix_3A
Text GLabel 1750 6500 0    50   Input ~ 0
Pix_1B
Text GLabel 1750 6750 0    50   Input ~ 0
Pix_1A
Text GLabel 1750 7000 0    50   Input ~ 0
TCO_1B
Text GLabel 1750 7250 0    50   Input ~ 0
TCO_1A
Text GLabel 5700 1700 0    50   Input ~ 0
TCO_4A
Text GLabel 5700 1800 0    50   Input ~ 0
TCO_4B
Text GLabel 5700 1900 0    50   Input ~ 0
Pix_6B
Text GLabel 5700 2000 0    50   Input ~ 0
Pix_6A
Text GLabel 5700 2200 0    50   Input ~ 0
Pix_4B
Text GLabel 5700 2300 0    50   Input ~ 0
Pix_4A
Text GLabel 5700 2500 0    50   Input ~ 0
Pix_2B
Text GLabel 5700 2600 0    50   Input ~ 0
Pix_2A
Text GLabel 5700 2800 0    50   Input ~ 0
TCO_2A
Text GLabel 5700 2700 0    50   Input ~ 0
TCO_2B
Text GLabel 1900 2750 2    50   Input ~ 0
TCO_1A
Text GLabel 1900 1650 2    50   Input ~ 0
TCO_3A
Text GLabel 1900 1750 2    50   Input ~ 0
TCO_3B
Text GLabel 1900 1850 2    50   Input ~ 0
Pix_5B
Text GLabel 1900 2150 2    50   Input ~ 0
Pix_3B
Text GLabel 1900 1950 2    50   Input ~ 0
Pix_5A
Text GLabel 1900 2250 2    50   Input ~ 0
Pix_3A
Text GLabel 1900 2450 2    50   Input ~ 0
Pix_1B
Text GLabel 1900 2550 2    50   Input ~ 0
Pix_1A
Text GLabel 1900 2650 2    50   Input ~ 0
TCO_1B
$Comp
L Connector_Generic:Conn_01x12 J12
U 1 1 5E4A2A7D
P 5900 2300
F 0 "J12" H 5980 2292 50  0000 L CNN
F 1 "Conn_01x12" H 5980 2201 50  0000 L CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x12_P2.00mm_Vertical" H 5900 2300 50  0001 C CNN
F 3 "~" H 5900 2300 50  0001 C CNN
	1    5900 2300
	1    0    0    1   
$EndComp
Text GLabel 1900 2050 2    50   Input ~ 0
PD
Text GLabel 1900 2350 2    50   Input ~ 0
BOT_PHIL
Text GLabel 5700 2400 0    50   Input ~ 0
TOP_PHIL
Text GLabel 5700 2100 0    50   Input ~ 0
LED
Text GLabel 3100 950  0    50   Input ~ 0
BOT_PHIL
$Comp
L Device:D_Photo D1
U 1 1 5E548D71
P 3300 950
F 0 "D1" H 3250 1245 50  0000 C CNN
F 1 "D_Photo" H 3250 1154 50  0000 C CNN
F 2 "Diode_SMD:D_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3250 950 50  0001 C CNN
F 3 "~" H 3250 950 50  0001 C CNN
	1    3300 950 
	1    0    0    -1  
$EndComp
Text GLabel 3400 950  2    50   Input ~ 0
PD
Text GLabel 4500 950  0    50   Input ~ 0
BOT_PHIL
$Comp
L Device:R R2
U 1 1 5E549FC6
P 4650 950
F 0 "R2" V 4443 950 50  0000 C CNN
F 1 "R" V 4534 950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4580 950 50  0001 C CNN
F 3 "~" H 4650 950 50  0001 C CNN
	1    4650 950 
	0    1    1    0   
$EndComp
Text GLabel 4800 950  2    50   Input ~ 0
TOP_PHIL
Text GLabel 5900 950  0    50   Input ~ 0
BOT_PHIL
$Comp
L Device:LED D2
U 1 1 5E54AFB2
P 6050 950
F 0 "D2" H 6043 1166 50  0000 C CNN
F 1 "LED" H 6043 1075 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6050 950 50  0001 C CNN
F 3 "~" H 6050 950 50  0001 C CNN
	1    6050 950 
	1    0    0    -1  
$EndComp
Text GLabel 6200 950  2    50   Input ~ 0
LED
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J23
U 1 1 5E54D934
P 3250 2150
F 0 "J23" H 3300 1625 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 3300 1716 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x06_P2.54mm_Vertical" H 3250 2150 50  0001 C CNN
F 3 "~" H 3250 2150 50  0001 C CNN
	1    3250 2150
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J24
U 1 1 5E54EC59
P 4500 2150
F 0 "J24" H 4550 1625 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 4550 1716 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x06_P2.54mm_Vertical" H 4500 2150 50  0001 C CNN
F 3 "~" H 4500 2150 50  0001 C CNN
	1    4500 2150
	-1   0    0    1   
$EndComp
Text GLabel 4700 2350 2    50   Input ~ 0
Pix_2A
Text GLabel 4700 2250 2    50   Input ~ 0
Pix_2B
Text GLabel 4700 2150 2    50   Input ~ 0
Pix_4A
Text GLabel 4700 2050 2    50   Input ~ 0
Pix_4B
Text GLabel 4700 1950 2    50   Input ~ 0
Pix_6A
Text GLabel 4700 1850 2    50   Input ~ 0
Pix_6B
Text GLabel 4200 2150 0    50   Input ~ 0
TCO_4A
Text GLabel 4200 2050 0    50   Input ~ 0
TCO_4B
Text GLabel 3450 2150 2    50   Input ~ 0
TCO_1B
Text GLabel 3450 2050 2    50   Input ~ 0
TCO_1A
Text GLabel 2950 2350 0    50   Input ~ 0
Pix_1A
Text GLabel 2950 2250 0    50   Input ~ 0
Pix_1B
Text GLabel 2950 2150 0    50   Input ~ 0
Pix_3A
Text GLabel 2950 2050 0    50   Input ~ 0
Pix_3B
Text GLabel 2950 1950 0    50   Input ~ 0
Pix_5A
Text GLabel 2950 1850 0    50   Input ~ 0
Pix_5B
$Comp
L Device:R R1
U 1 1 5E553EDE
P 4050 2700
F 0 "R1" H 4120 2746 50  0000 L CNN
F 1 "R" H 4120 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3980 2700 50  0001 C CNN
F 3 "~" H 4050 2700 50  0001 C CNN
	1    4050 2700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4200 2850 4050 2850
Wire Wire Line
	4200 2350 4200 2850
Wire Wire Line
	4050 2550 4050 2250
Wire Wire Line
	4200 2250 4050 2250
NoConn ~ 3450 1850
NoConn ~ 3450 1950
NoConn ~ 4200 1850
NoConn ~ 4200 1950
NoConn ~ 3450 2250
NoConn ~ 3450 2350
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5E5A2267
P 8950 3150
F 0 "H3" V 9187 3153 50  0000 C CNN
F 1 "MountingHole_Pad" V 9096 3153 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.7mm_Pad_Via" H 8950 3150 50  0001 C CNN
F 3 "~" H 8950 3150 50  0001 C CNN
	1    8950 3150
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5E5A2D95
P 8950 3550
F 0 "H4" V 9187 3553 50  0000 C CNN
F 1 "MountingHole_Pad" V 9096 3553 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.7mm_Pad_Via" H 8950 3550 50  0001 C CNN
F 3 "~" H 8950 3550 50  0001 C CNN
	1    8950 3550
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H5
U 1 1 5E5A3904
P 9650 3150
F 0 "H5" V 9887 3153 50  0000 C CNN
F 1 "MountingHole_Pad" V 9796 3153 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.7mm_Pad_Via" H 9650 3150 50  0001 C CNN
F 3 "~" H 9650 3150 50  0001 C CNN
	1    9650 3150
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H7
U 1 1 5E5A4351
P 10350 3150
F 0 "H7" V 10587 3153 50  0000 C CNN
F 1 "MountingHole_Pad" V 10496 3153 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.7mm_Pad_Via" H 10350 3150 50  0001 C CNN
F 3 "~" H 10350 3150 50  0001 C CNN
	1    10350 3150
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5E5A50CA
P 8200 3150
F 0 "H1" V 8437 3153 50  0000 C CNN
F 1 "MountingHole_Pad" V 8346 3153 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.7mm_Pad_Via" H 8200 3150 50  0001 C CNN
F 3 "~" H 8200 3150 50  0001 C CNN
	1    8200 3150
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H6
U 1 1 5E5A5F0F
P 9650 3550
F 0 "H6" V 9887 3553 50  0000 C CNN
F 1 "MountingHole_Pad" V 9796 3553 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.7mm_Pad_Via" H 9650 3550 50  0001 C CNN
F 3 "~" H 9650 3550 50  0001 C CNN
	1    9650 3550
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H8
U 1 1 5E5A66DA
P 10350 3550
F 0 "H8" V 10587 3553 50  0000 C CNN
F 1 "MountingHole_Pad" V 10496 3553 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.7mm_Pad_Via" H 10350 3550 50  0001 C CNN
F 3 "~" H 10350 3550 50  0001 C CNN
	1    10350 3550
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5E5A6F12
P 8200 3550
F 0 "H2" V 8437 3553 50  0000 C CNN
F 1 "MountingHole_Pad" V 8346 3553 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.7mm_Pad_Via" H 8200 3550 50  0001 C CNN
F 3 "~" H 8200 3550 50  0001 C CNN
	1    8200 3550
	0    -1   -1   0   
$EndComp
Text GLabel 8300 3150 2    50   Input ~ 0
mountHole
Text GLabel 8300 3550 2    50   Input ~ 0
mountHole
Text GLabel 9050 3550 2    50   Input ~ 0
mountHole
Text GLabel 9050 3150 2    50   Input ~ 0
mountHole
Text GLabel 9750 3150 2    50   Input ~ 0
mountHole
Text GLabel 10450 3150 2    50   Input ~ 0
mountHole
Text GLabel 10450 3550 2    50   Input ~ 0
mountHole
Text GLabel 9750 3550 2    50   Input ~ 0
mountHole
Text GLabel 2150 3450 0    50   Input ~ 0
TCO_3A
Text GLabel 2150 3550 0    50   Input ~ 0
TCO_3B
Text GLabel 5650 3450 0    50   Input ~ 0
TCO_4A
Text GLabel 5650 3550 0    50   Input ~ 0
TCO_4B
Text GLabel 2250 3550 2    50   Input ~ 0
TCO_1B
Text GLabel 2250 3450 2    50   Input ~ 0
TCO_1A
Text GLabel 5800 3550 2    50   Input ~ 0
TCO_2B
Text GLabel 5800 3450 2    50   Input ~ 0
TCO_2A
Wire Wire Line
	2150 3450 2250 3450
Wire Wire Line
	2150 3550 2250 3550
Wire Wire Line
	5650 3450 5800 3450
Wire Wire Line
	5800 3550 5650 3550
$EndSCHEMATC
