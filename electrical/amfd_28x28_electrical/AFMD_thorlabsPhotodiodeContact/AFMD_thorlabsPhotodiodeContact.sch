EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x01 J1
U 1 1 62225454
P 3000 2100
F 0 "J1" H 3080 2142 50  0000 L CNN
F 1 "Conn_01x01" H 3080 2051 50  0000 L CNN
F 2 "myParts:AFMD_28x28_contactPad" H 3000 2100 50  0001 C CNN
F 3 "~" H 3000 2100 50  0001 C CNN
	1    3000 2100
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J2
U 1 1 62225BDD
P 3000 2450
F 0 "J2" H 3080 2492 50  0000 L CNN
F 1 "Conn_01x01" H 3080 2401 50  0000 L CNN
F 2 "myParts:AFMD_28x28_contactPad" H 3000 2450 50  0001 C CNN
F 3 "~" H 3000 2450 50  0001 C CNN
	1    3000 2450
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J3
U 1 1 62225F6A
P 3000 2800
F 0 "J3" H 3080 2842 50  0000 L CNN
F 1 "Conn_01x01" H 3080 2751 50  0000 L CNN
F 2 "myParts:AFMD_28x28_contactPad" H 3000 2800 50  0001 C CNN
F 3 "~" H 3000 2800 50  0001 C CNN
	1    3000 2800
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J4
U 1 1 622265EB
P 3000 3150
F 0 "J4" H 3080 3192 50  0000 L CNN
F 1 "Conn_01x01" H 3080 3101 50  0000 L CNN
F 2 "myParts:AFMD_28x28_contactPad" H 3000 3150 50  0001 C CNN
F 3 "~" H 3000 3150 50  0001 C CNN
	1    3000 3150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J12
U 1 1 6222A68B
P 5750 3150
F 0 "J12" H 5830 3192 50  0000 L CNN
F 1 "Conn_01x01" H 5830 3101 50  0000 L CNN
F 2 "myParts:AFMD_28x28_contactPad" H 5750 3150 50  0001 C CNN
F 3 "~" H 5750 3150 50  0001 C CNN
	1    5750 3150
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J11
U 1 1 6222A691
P 5750 2800
F 0 "J11" H 5830 2842 50  0000 L CNN
F 1 "Conn_01x01" H 5830 2751 50  0000 L CNN
F 2 "myParts:AFMD_28x28_contactPad" H 5750 2800 50  0001 C CNN
F 3 "~" H 5750 2800 50  0001 C CNN
	1    5750 2800
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J10
U 1 1 6222A697
P 5750 2450
F 0 "J10" H 5830 2492 50  0000 L CNN
F 1 "Conn_01x01" H 5830 2401 50  0000 L CNN
F 2 "myParts:AFMD_28x28_contactPad" H 5750 2450 50  0001 C CNN
F 3 "~" H 5750 2450 50  0001 C CNN
	1    5750 2450
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J9
U 1 1 6222A69D
P 5750 2100
F 0 "J9" H 5830 2142 50  0000 L CNN
F 1 "Conn_01x01" H 5830 2051 50  0000 L CNN
F 2 "myParts:AFMD_28x28_contactPad" H 5750 2100 50  0001 C CNN
F 3 "~" H 5750 2100 50  0001 C CNN
	1    5750 2100
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J6
U 1 1 6222A7F6
P 4300 1800
F 0 "J6" V 4172 1880 50  0000 L CNN
F 1 "Conn_01x01" V 4263 1880 50  0000 L CNN
F 2 "myParts:AFMD_28x28_contactPad" H 4300 1800 50  0001 C CNN
F 3 "~" H 4300 1800 50  0001 C CNN
	1    4300 1800
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J7
U 1 1 6222B2BF
P 4300 3500
F 0 "J7" V 4264 3412 50  0000 R CNN
F 1 "Conn_01x01" V 4173 3412 50  0000 R CNN
F 2 "myParts:AFMD_28x28_contactPad" H 4300 3500 50  0001 C CNN
F 3 "~" H 4300 3500 50  0001 C CNN
	1    4300 3500
	0    -1   -1   0   
$EndComp
Text GLabel 2800 2100 0    50   Input ~ 0
pixel_8_contact
Text GLabel 2800 2450 0    50   Input ~ 0
pixel_6_contact
Text GLabel 2800 2800 0    50   Input ~ 0
pixel_4_contact
Text GLabel 2800 3150 0    50   Input ~ 0
pixel_2_contact
Text GLabel 5950 2100 2    50   Input ~ 0
pixel_7_contact
Text GLabel 5950 2450 2    50   Input ~ 0
pixel_5_contact
Text GLabel 5950 2800 2    50   Input ~ 0
pixel_3_contact
Text GLabel 5950 3150 2    50   Input ~ 0
pixel_1_contact
Text GLabel 4300 1600 2    50   Input ~ 0
TCO_contact
Text GLabel 4300 3700 2    50   Input ~ 0
TCO_contact
$Comp
L Connector_Generic:Conn_01x03 J8
U 1 1 6222D0AB
P 4350 2200
F 0 "J8" H 4430 2242 50  0000 L CNN
F 1 "Conn_01x03" H 4430 2151 50  0000 L CNN
F 2 "myParts:thorlabs_STO5S_photodiodeSocket" H 4350 2200 50  0001 C CNN
F 3 "~" H 4350 2200 50  0001 C CNN
	1    4350 2200
	1    0    0    -1  
$EndComp
Text GLabel 4150 2100 0    50   Input ~ 0
pixel_4_contact
Text GLabel 4150 2300 0    50   Input ~ 0
TCO_contact
$Comp
L Connector_Generic:Conn_01x03 J5
U 1 1 6222DDC3
P 3800 3000
F 0 "J5" H 3718 3317 50  0000 C CNN
F 1 "Conn_01x03" H 3718 3226 50  0000 C CNN
F 2 "myParts:thorlabs_STO5S_photodiodeSocket" H 3800 3000 50  0001 C CNN
F 3 "~" H 3800 3000 50  0001 C CNN
	1    3800 3000
	-1   0    0    -1  
$EndComp
Text GLabel 4000 2900 2    50   Input ~ 0
pixel_5_contact
Text GLabel 4000 3100 2    50   Input ~ 0
TCO_contact
NoConn ~ 4150 2200
NoConn ~ 4000 3000
Text GLabel 3050 4650 0    50   Input ~ 0
pixel_8_contact
Text GLabel 3050 4750 0    50   Input ~ 0
pixel_6_contact
Text GLabel 3050 4850 0    50   Input ~ 0
pixel_2_contact
NoConn ~ 3050 4650
NoConn ~ 3050 4750
NoConn ~ 3050 4850
Text GLabel 3050 5250 0    50   Input ~ 0
pixel_7_contact
Text GLabel 3050 5150 0    50   Input ~ 0
pixel_3_contact
Text GLabel 3050 5050 0    50   Input ~ 0
pixel_1_contact
NoConn ~ 3050 5050
NoConn ~ 3050 5150
NoConn ~ 3050 5250
$EndSCHEMATC
