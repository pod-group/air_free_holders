EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J9
U 1 1 619B1345
P 3400 2800
F 0 "J9" H 3450 2275 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 3450 2366 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x06_P2.54mm_Vertical" H 3400 2800 50  0001 C CNN
F 3 "~" H 3400 2800 50  0001 C CNN
	1    3400 2800
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J10
U 1 1 619B3E3C
P 5200 2800
F 0 "J10" H 5250 2275 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 5250 2366 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x06_P2.54mm_Vertical" H 5200 2800 50  0001 C CNN
F 3 "~" H 5200 2800 50  0001 C CNN
	1    5200 2800
	1    0    0    1   
$EndComp
Text GLabel 3700 2700 2    50   Input ~ 0
TCO_A
Text GLabel 3700 2800 2    50   Input ~ 0
TCO_B
Text GLabel 5000 2800 0    50   Input ~ 0
TCO_A
Text GLabel 5000 2700 0    50   Input ~ 0
TCO_B
Text GLabel 3200 3000 0    50   Input ~ 0
pixel_2_A
Text GLabel 3200 2900 0    50   Input ~ 0
pixel_2_B
Text GLabel 3200 2800 0    50   Input ~ 0
pixel_4_A
Text GLabel 3200 2700 0    50   Input ~ 0
pixel_4_B
Text GLabel 3200 2600 0    50   Input ~ 0
pixel_6_A
Text GLabel 3200 2500 0    50   Input ~ 0
pixel_6_B
Text GLabel 3700 2600 2    50   Input ~ 0
pixel_8_A
Text GLabel 3700 2500 2    50   Input ~ 0
pixel_8_B
Text GLabel 5000 2600 0    50   Input ~ 0
pixel_7_A
Text GLabel 5000 2500 0    50   Input ~ 0
pixel_7_B
Text GLabel 5500 3000 2    50   Input ~ 0
pixel_1_A
Text GLabel 5500 2900 2    50   Input ~ 0
pixel_1_B
Text GLabel 5500 2800 2    50   Input ~ 0
pixel_3_A
Text GLabel 5500 2700 2    50   Input ~ 0
pixel_3_B
Text GLabel 5500 2600 2    50   Input ~ 0
pixel_5_A
Text GLabel 5500 2500 2    50   Input ~ 0
pixel_5_B
$Comp
L Connector_Generic:Conn_01x01 J1
U 1 1 619B9EEF
P 1150 1900
F 0 "J1" H 1450 1850 50  0000 C CNN
F 1 "Conn_01x01" H 1450 1950 50  0000 C CNN
F 2 "myParts:pogoPin" H 1150 1900 50  0001 C CNN
F 3 "~" H 1150 1900 50  0001 C CNN
	1    1150 1900
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J2
U 1 1 619BAF08
P 1150 2100
F 0 "J2" H 1450 2050 50  0000 C CNN
F 1 "Conn_01x01" H 1450 2150 50  0000 C CNN
F 2 "myParts:pogoPin" H 1150 2100 50  0001 C CNN
F 3 "~" H 1150 2100 50  0001 C CNN
	1    1150 2100
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J3
U 1 1 619BE118
P 1150 2400
F 0 "J3" H 1450 2350 50  0000 C CNN
F 1 "Conn_01x01" H 1450 2450 50  0000 C CNN
F 2 "myParts:pogoPin" H 1150 2400 50  0001 C CNN
F 3 "~" H 1150 2400 50  0001 C CNN
	1    1150 2400
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J4
U 1 1 619BE11E
P 1150 2600
F 0 "J4" H 1450 2550 50  0000 C CNN
F 1 "Conn_01x01" H 1450 2650 50  0000 C CNN
F 2 "myParts:pogoPin" H 1150 2600 50  0001 C CNN
F 3 "~" H 1150 2600 50  0001 C CNN
	1    1150 2600
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J5
U 1 1 619BE8B2
P 1150 2900
F 0 "J5" H 1450 2850 50  0000 C CNN
F 1 "Conn_01x01" H 1450 2950 50  0000 C CNN
F 2 "myParts:pogoPin" H 1150 2900 50  0001 C CNN
F 3 "~" H 1150 2900 50  0001 C CNN
	1    1150 2900
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J6
U 1 1 619BE8B8
P 1150 3100
F 0 "J6" H 1450 3050 50  0000 C CNN
F 1 "Conn_01x01" H 1450 3150 50  0000 C CNN
F 2 "myParts:pogoPin" H 1150 3100 50  0001 C CNN
F 3 "~" H 1150 3100 50  0001 C CNN
	1    1150 3100
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J7
U 1 1 619BF40C
P 1150 3400
F 0 "J7" H 1450 3350 50  0000 C CNN
F 1 "Conn_01x01" H 1450 3450 50  0000 C CNN
F 2 "myParts:pogoPin" H 1150 3400 50  0001 C CNN
F 3 "~" H 1150 3400 50  0001 C CNN
	1    1150 3400
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J8
U 1 1 619BF412
P 1150 3600
F 0 "J8" H 1450 3550 50  0000 C CNN
F 1 "Conn_01x01" H 1450 3650 50  0000 C CNN
F 2 "myParts:pogoPin" H 1150 3600 50  0001 C CNN
F 3 "~" H 1150 3600 50  0001 C CNN
	1    1150 3600
	-1   0    0    1   
$EndComp
Text GLabel 1350 3600 2    50   Input ~ 0
pixel_2_A
Text GLabel 1350 3400 2    50   Input ~ 0
pixel_2_B
Text GLabel 1350 3100 2    50   Input ~ 0
pixel_4_A
Text GLabel 1350 2900 2    50   Input ~ 0
pixel_4_B
Text GLabel 1350 2600 2    50   Input ~ 0
pixel_6_A
Text GLabel 1350 2400 2    50   Input ~ 0
pixel_6_B
Text GLabel 1350 2100 2    50   Input ~ 0
pixel_8_A
Text GLabel 1350 1900 2    50   Input ~ 0
pixel_8_B
$Comp
L Connector_Generic:Conn_01x01 J11
U 1 1 619C1E0B
P 7700 1900
F 0 "J11" H 7780 1942 50  0000 L CNN
F 1 "Conn_01x01" H 7780 1851 50  0000 L CNN
F 2 "myParts:pogoPin" H 7700 1900 50  0001 C CNN
F 3 "~" H 7700 1900 50  0001 C CNN
	1    7700 1900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J12
U 1 1 619C3078
P 7700 2100
F 0 "J12" H 7780 2142 50  0000 L CNN
F 1 "Conn_01x01" H 7780 2051 50  0000 L CNN
F 2 "myParts:pogoPin" H 7700 2100 50  0001 C CNN
F 3 "~" H 7700 2100 50  0001 C CNN
	1    7700 2100
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J13
U 1 1 619C3CE0
P 7700 2400
F 0 "J13" H 7780 2442 50  0000 L CNN
F 1 "Conn_01x01" H 7780 2351 50  0000 L CNN
F 2 "myParts:pogoPin" H 7700 2400 50  0001 C CNN
F 3 "~" H 7700 2400 50  0001 C CNN
	1    7700 2400
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J14
U 1 1 619C3CE6
P 7700 2600
F 0 "J14" H 7780 2642 50  0000 L CNN
F 1 "Conn_01x01" H 7780 2551 50  0000 L CNN
F 2 "myParts:pogoPin" H 7700 2600 50  0001 C CNN
F 3 "~" H 7700 2600 50  0001 C CNN
	1    7700 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J15
U 1 1 619C456A
P 7700 2900
F 0 "J15" H 7780 2942 50  0000 L CNN
F 1 "Conn_01x01" H 7780 2851 50  0000 L CNN
F 2 "myParts:pogoPin" H 7700 2900 50  0001 C CNN
F 3 "~" H 7700 2900 50  0001 C CNN
	1    7700 2900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J16
U 1 1 619C4570
P 7700 3100
F 0 "J16" H 7780 3142 50  0000 L CNN
F 1 "Conn_01x01" H 7780 3051 50  0000 L CNN
F 2 "myParts:pogoPin" H 7700 3100 50  0001 C CNN
F 3 "~" H 7700 3100 50  0001 C CNN
	1    7700 3100
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J17
U 1 1 619C5344
P 7700 3400
F 0 "J17" H 7780 3442 50  0000 L CNN
F 1 "Conn_01x01" H 7780 3351 50  0000 L CNN
F 2 "myParts:pogoPin" H 7700 3400 50  0001 C CNN
F 3 "~" H 7700 3400 50  0001 C CNN
	1    7700 3400
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J18
U 1 1 619C534A
P 7700 3600
F 0 "J18" H 7780 3642 50  0000 L CNN
F 1 "Conn_01x01" H 7780 3551 50  0000 L CNN
F 2 "myParts:pogoPin" H 7700 3600 50  0001 C CNN
F 3 "~" H 7700 3600 50  0001 C CNN
	1    7700 3600
	1    0    0    -1  
$EndComp
Text GLabel 7500 3600 0    50   Input ~ 0
pixel_1_A
Text GLabel 7500 3400 0    50   Input ~ 0
pixel_1_B
Text GLabel 7500 3100 0    50   Input ~ 0
pixel_3_A
Text GLabel 7500 2900 0    50   Input ~ 0
pixel_3_B
Text GLabel 7500 2600 0    50   Input ~ 0
pixel_5_A
Text GLabel 7500 2400 0    50   Input ~ 0
pixel_5_B
Text GLabel 7500 2100 0    50   Input ~ 0
pixel_7_A
Text GLabel 7500 1900 0    50   Input ~ 0
pixel_7_B
NoConn ~ 3700 2900
NoConn ~ 3700 3000
NoConn ~ 5000 3000
NoConn ~ 5000 2900
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 619C6550
P 3500 4500
F 0 "H1" V 3454 4650 50  0000 L CNN
F 1 "MountingHole_Pad" V 3545 4650 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_2.7mm_M2.5_Pad_Via" H 3500 4500 50  0001 C CNN
F 3 "~" H 3500 4500 50  0001 C CNN
	1    3500 4500
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 619C8258
P 3500 4750
F 0 "H2" V 3454 4900 50  0000 L CNN
F 1 "MountingHole_Pad" V 3545 4900 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_2.7mm_M2.5_Pad_Via" H 3500 4750 50  0001 C CNN
F 3 "~" H 3500 4750 50  0001 C CNN
	1    3500 4750
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 619C91C3
P 3500 5000
F 0 "H3" V 3454 5150 50  0000 L CNN
F 1 "MountingHole_Pad" V 3545 5150 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_2.7mm_M2.5_Pad_Via" H 3500 5000 50  0001 C CNN
F 3 "~" H 3500 5000 50  0001 C CNN
	1    3500 5000
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 619C91C9
P 3500 5250
F 0 "H4" V 3454 5400 50  0000 L CNN
F 1 "MountingHole_Pad" V 3545 5400 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_2.7mm_M2.5_Pad_Via" H 3500 5250 50  0001 C CNN
F 3 "~" H 3500 5250 50  0001 C CNN
	1    3500 5250
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H5
U 1 1 619C9A05
P 3500 5500
F 0 "H5" V 3454 5650 50  0000 L CNN
F 1 "MountingHole_Pad" V 3545 5650 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_2.7mm_M2.5_Pad_Via" H 3500 5500 50  0001 C CNN
F 3 "~" H 3500 5500 50  0001 C CNN
	1    3500 5500
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H6
U 1 1 619C9A0B
P 3500 5750
F 0 "H6" V 3454 5900 50  0000 L CNN
F 1 "MountingHole_Pad" V 3545 5900 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_2.7mm_M2.5_Pad_Via" H 3500 5750 50  0001 C CNN
F 3 "~" H 3500 5750 50  0001 C CNN
	1    3500 5750
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H7
U 1 1 619CA2FB
P 3500 6000
F 0 "H7" V 3454 6150 50  0000 L CNN
F 1 "MountingHole_Pad" V 3545 6150 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_2.7mm_M2.5_Pad_Via" H 3500 6000 50  0001 C CNN
F 3 "~" H 3500 6000 50  0001 C CNN
	1    3500 6000
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H8
U 1 1 619CA301
P 3500 6250
F 0 "H8" V 3454 6400 50  0000 L CNN
F 1 "MountingHole_Pad" V 3545 6400 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_2.7mm_M2.5_Pad_Via" H 3500 6250 50  0001 C CNN
F 3 "~" H 3500 6250 50  0001 C CNN
	1    3500 6250
	0    1    1    0   
$EndComp
Text GLabel 3400 4500 0    50   Input ~ 0
mountingHole
Text GLabel 3400 4750 0    50   Input ~ 0
mountingHole
Text GLabel 3400 5000 0    50   Input ~ 0
mountingHole
Text GLabel 3400 5250 0    50   Input ~ 0
mountingHole
Text GLabel 3400 5500 0    50   Input ~ 0
mountingHole
Text GLabel 3400 5750 0    50   Input ~ 0
mountingHole
Text GLabel 3400 6000 0    50   Input ~ 0
mountingHole
Text GLabel 3400 6250 0    50   Input ~ 0
mountingHole
$Comp
L Connector_Generic:Conn_01x01 J19
U 1 1 619E2F73
P 4150 1200
F 0 "J19" H 4100 1450 50  0000 L CNN
F 1 "Conn_01x01" H 3950 1350 50  0000 L CNN
F 2 "myParts:pogoPin" H 4150 1200 50  0001 C CNN
F 3 "~" H 4150 1200 50  0001 C CNN
	1    4150 1200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J21
U 1 1 619E37B7
P 4700 1200
F 0 "J21" H 4750 950 50  0000 C CNN
F 1 "Conn_01x01" H 4700 1050 50  0000 C CNN
F 2 "myParts:pogoPin" H 4700 1200 50  0001 C CNN
F 3 "~" H 4700 1200 50  0001 C CNN
	1    4700 1200
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J20
U 1 1 619E5F6C
P 4150 3900
F 0 "J20" H 4100 4150 50  0000 L CNN
F 1 "Conn_01x01" H 3950 4050 50  0000 L CNN
F 2 "myParts:pogoPin" H 4150 3900 50  0001 C CNN
F 3 "~" H 4150 3900 50  0001 C CNN
	1    4150 3900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J22
U 1 1 619E5F72
P 4700 3900
F 0 "J22" H 4750 3650 50  0000 C CNN
F 1 "Conn_01x01" H 4700 3750 50  0000 C CNN
F 2 "myParts:pogoPin" H 4700 3900 50  0001 C CNN
F 3 "~" H 4700 3900 50  0001 C CNN
	1    4700 3900
	-1   0    0    1   
$EndComp
Text GLabel 3950 1200 0    50   Input ~ 0
TCO_A
Text GLabel 4900 1200 2    50   Input ~ 0
TCO_B
Text GLabel 4900 3900 2    50   Input ~ 0
TCO_A
Text GLabel 3950 3900 0    50   Input ~ 0
TCO_B
$EndSCHEMATC
