EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x01 J1
U 1 1 621D38AF
P 2550 2150
F 0 "J1" H 2630 2192 50  0000 L CNN
F 1 "Conn_01x01" H 2630 2101 50  0000 L CNN
F 2 "myParts:pogoPin" H 2550 2150 50  0001 C CNN
F 3 "~" H 2550 2150 50  0001 C CNN
	1    2550 2150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J2
U 1 1 621D4CE0
P 2550 2500
F 0 "J2" H 2630 2542 50  0000 L CNN
F 1 "Conn_01x01" H 2630 2451 50  0000 L CNN
F 2 "myParts:pogoPin" H 2550 2500 50  0001 C CNN
F 3 "~" H 2550 2500 50  0001 C CNN
	1    2550 2500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J3
U 1 1 621D54D8
P 2550 2850
F 0 "J3" H 2630 2892 50  0000 L CNN
F 1 "Conn_01x01" H 2630 2801 50  0000 L CNN
F 2 "myParts:pogoPin" H 2550 2850 50  0001 C CNN
F 3 "~" H 2550 2850 50  0001 C CNN
	1    2550 2850
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J4
U 1 1 621D58A4
P 2550 3250
F 0 "J4" H 2630 3292 50  0000 L CNN
F 1 "Conn_01x01" H 2630 3201 50  0000 L CNN
F 2 "myParts:pogoPin" H 2550 3250 50  0001 C CNN
F 3 "~" H 2550 3250 50  0001 C CNN
	1    2550 3250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J5
U 1 1 621D5ED8
P 2550 3600
F 0 "J5" H 2630 3642 50  0000 L CNN
F 1 "Conn_01x01" H 2630 3551 50  0000 L CNN
F 2 "myParts:pogoPin" H 2550 3600 50  0001 C CNN
F 3 "~" H 2550 3600 50  0001 C CNN
	1    2550 3600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J6
U 1 1 621D643D
P 2550 3950
F 0 "J6" H 2630 3992 50  0000 L CNN
F 1 "Conn_01x01" H 2630 3901 50  0000 L CNN
F 2 "myParts:pogoPin" H 2550 3950 50  0001 C CNN
F 3 "~" H 2550 3950 50  0001 C CNN
	1    2550 3950
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J7
U 1 1 621D6C0E
P 2550 4300
F 0 "J7" H 2630 4342 50  0000 L CNN
F 1 "Conn_01x01" H 2630 4251 50  0000 L CNN
F 2 "myParts:pogoPin" H 2550 4300 50  0001 C CNN
F 3 "~" H 2550 4300 50  0001 C CNN
	1    2550 4300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J8
U 1 1 621D7154
P 2550 4650
F 0 "J8" H 2630 4692 50  0000 L CNN
F 1 "Conn_01x01" H 2630 4601 50  0000 L CNN
F 2 "myParts:pogoPin" H 2550 4650 50  0001 C CNN
F 3 "~" H 2550 4650 50  0001 C CNN
	1    2550 4650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J20
U 1 1 621E9AC9
P 6650 4650
F 0 "J20" H 6730 4692 50  0000 L CNN
F 1 "Conn_01x01" H 6730 4601 50  0000 L CNN
F 2 "myParts:pogoPin" H 6650 4650 50  0001 C CNN
F 3 "~" H 6650 4650 50  0001 C CNN
	1    6650 4650
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J19
U 1 1 621E9ACF
P 6650 4300
F 0 "J19" H 6730 4342 50  0000 L CNN
F 1 "Conn_01x01" H 6730 4251 50  0000 L CNN
F 2 "myParts:pogoPin" H 6650 4300 50  0001 C CNN
F 3 "~" H 6650 4300 50  0001 C CNN
	1    6650 4300
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J18
U 1 1 621E9AD5
P 6650 3950
F 0 "J18" H 6730 3992 50  0000 L CNN
F 1 "Conn_01x01" H 6730 3901 50  0000 L CNN
F 2 "myParts:pogoPin" H 6650 3950 50  0001 C CNN
F 3 "~" H 6650 3950 50  0001 C CNN
	1    6650 3950
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J17
U 1 1 621E9ADB
P 6650 3550
F 0 "J17" H 6730 3592 50  0000 L CNN
F 1 "Conn_01x01" H 6730 3501 50  0000 L CNN
F 2 "myParts:pogoPin" H 6650 3550 50  0001 C CNN
F 3 "~" H 6650 3550 50  0001 C CNN
	1    6650 3550
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J16
U 1 1 621E9AE1
P 6650 3200
F 0 "J16" H 6730 3242 50  0000 L CNN
F 1 "Conn_01x01" H 6730 3151 50  0000 L CNN
F 2 "myParts:pogoPin" H 6650 3200 50  0001 C CNN
F 3 "~" H 6650 3200 50  0001 C CNN
	1    6650 3200
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J15
U 1 1 621E9AE7
P 6650 2850
F 0 "J15" H 6730 2892 50  0000 L CNN
F 1 "Conn_01x01" H 6730 2801 50  0000 L CNN
F 2 "myParts:pogoPin" H 6650 2850 50  0001 C CNN
F 3 "~" H 6650 2850 50  0001 C CNN
	1    6650 2850
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J14
U 1 1 621E9AED
P 6650 2500
F 0 "J14" H 6730 2542 50  0000 L CNN
F 1 "Conn_01x01" H 6730 2451 50  0000 L CNN
F 2 "myParts:pogoPin" H 6650 2500 50  0001 C CNN
F 3 "~" H 6650 2500 50  0001 C CNN
	1    6650 2500
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J13
U 1 1 621E9AF3
P 6650 2150
F 0 "J13" H 6730 2192 50  0000 L CNN
F 1 "Conn_01x01" H 6730 2101 50  0000 L CNN
F 2 "myParts:pogoPin" H 6650 2150 50  0001 C CNN
F 3 "~" H 6650 2150 50  0001 C CNN
	1    6650 2150
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J12
U 1 1 621E9DD6
P 5000 4900
F 0 "J12" V 4964 4812 50  0000 R CNN
F 1 "Conn_01x01" V 4873 4812 50  0000 R CNN
F 2 "myParts:pogoPin" H 5000 4900 50  0001 C CNN
F 3 "~" H 5000 4900 50  0001 C CNN
	1    5000 4900
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J10
U 1 1 621EB08A
P 4250 4900
F 0 "J10" V 4214 4812 50  0000 R CNN
F 1 "Conn_01x01" V 4123 4812 50  0000 R CNN
F 2 "myParts:pogoPin" H 4250 4900 50  0001 C CNN
F 3 "~" H 4250 4900 50  0001 C CNN
	1    4250 4900
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J9
U 1 1 621EC6B4
P 4250 1950
F 0 "J9" V 4214 1862 50  0000 R CNN
F 1 "Conn_01x01" V 4123 1862 50  0000 R CNN
F 2 "myParts:pogoPin" H 4250 1950 50  0001 C CNN
F 3 "~" H 4250 1950 50  0001 C CNN
	1    4250 1950
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J11
U 1 1 621EC6BA
P 5000 1950
F 0 "J11" V 4964 1862 50  0000 R CNN
F 1 "Conn_01x01" V 4873 1862 50  0000 R CNN
F 2 "myParts:pogoPin" H 5000 1950 50  0001 C CNN
F 3 "~" H 5000 1950 50  0001 C CNN
	1    5000 1950
	0    1    1    0   
$EndComp
Text GLabel 2350 2500 0    50   Input ~ 0
pixel_8_A
Text GLabel 2350 2150 0    50   Input ~ 0
pixel_8_B
Text GLabel 2350 2850 0    50   Input ~ 0
pixel_6_B
Text GLabel 2350 3250 0    50   Input ~ 0
pixel_6_A
Text GLabel 2350 3950 0    50   Input ~ 0
pixel_4_A
Text GLabel 2350 3600 0    50   Input ~ 0
pixel_4_B
Text GLabel 2350 4650 0    50   Input ~ 0
pixel_2_A
Text GLabel 2350 4300 0    50   Input ~ 0
pixel_2_B
Text GLabel 4250 5100 3    50   Input ~ 0
TCO_B
Text GLabel 5000 5100 3    50   Input ~ 0
TCO_A
Text GLabel 4250 1750 1    50   Input ~ 0
TCO_A
Text GLabel 5000 1750 1    50   Input ~ 0
TCO_B
Text GLabel 6850 2150 2    50   Input ~ 0
pixel_7_B
Text GLabel 6850 2500 2    50   Input ~ 0
pixel_7_A
Text GLabel 6850 3200 2    50   Input ~ 0
pixel_5_A
Text GLabel 6850 2850 2    50   Input ~ 0
pixel_5_B
Text GLabel 6850 3950 2    50   Input ~ 0
pixel_3_A
Text GLabel 6850 3550 2    50   Input ~ 0
pixel_3_B
Text GLabel 6850 4650 2    50   Input ~ 0
pixel_1_A
Text GLabel 6850 4300 2    50   Input ~ 0
pixel_1_B
$Comp
L Device:D_Photo D1
U 1 1 621F11DB
P 3900 2350
F 0 "D1" H 3850 2134 50  0000 C CNN
F 1 "D_Photo" H 3850 2225 50  0000 C CNN
F 2 "myParts:BPW_34_photodiode" H 3850 2350 50  0001 C CNN
F 3 "~" H 3850 2350 50  0001 C CNN
	1    3900 2350
	-1   0    0    1   
$EndComp
$Comp
L Device:D_Photo D2
U 1 1 621F33D4
P 3900 3050
F 0 "D2" H 3850 2834 50  0000 C CNN
F 1 "D_Photo" H 3850 2925 50  0000 C CNN
F 2 "myParts:BPW_34_photodiode" H 3850 3050 50  0001 C CNN
F 3 "~" H 3850 3050 50  0001 C CNN
	1    3900 3050
	-1   0    0    1   
$EndComp
$Comp
L Device:D_Photo D3
U 1 1 621F416A
P 3900 3800
F 0 "D3" H 3850 3584 50  0000 C CNN
F 1 "D_Photo" H 3850 3675 50  0000 C CNN
F 2 "myParts:BPW_34_photodiode" H 3850 3800 50  0001 C CNN
F 3 "~" H 3850 3800 50  0001 C CNN
	1    3900 3800
	-1   0    0    1   
$EndComp
$Comp
L Device:D_Photo D4
U 1 1 621F498D
P 3900 4500
F 0 "D4" H 3850 4284 50  0000 C CNN
F 1 "D_Photo" H 3850 4375 50  0000 C CNN
F 2 "myParts:BPW_34_photodiode" H 3850 4500 50  0001 C CNN
F 3 "~" H 3850 4500 50  0001 C CNN
	1    3900 4500
	-1   0    0    1   
$EndComp
$Comp
L Device:D_Photo D8
U 1 1 621F4EA9
P 5350 4500
F 0 "D8" H 5300 4795 50  0000 C CNN
F 1 "D_Photo" H 5300 4704 50  0000 C CNN
F 2 "myParts:BPW_34_photodiode" H 5300 4500 50  0001 C CNN
F 3 "~" H 5300 4500 50  0001 C CNN
	1    5350 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Photo D7
U 1 1 621F607B
P 5350 3800
F 0 "D7" H 5300 4095 50  0000 C CNN
F 1 "D_Photo" H 5300 4004 50  0000 C CNN
F 2 "myParts:BPW_34_photodiode" H 5300 3800 50  0001 C CNN
F 3 "~" H 5300 3800 50  0001 C CNN
	1    5350 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Photo D6
U 1 1 621F691A
P 5350 3050
F 0 "D6" H 5300 3345 50  0000 C CNN
F 1 "D_Photo" H 5300 3254 50  0000 C CNN
F 2 "myParts:BPW_34_photodiode" H 5300 3050 50  0001 C CNN
F 3 "~" H 5300 3050 50  0001 C CNN
	1    5350 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Photo D5
U 1 1 621F7615
P 5350 2350
F 0 "D5" H 5300 2645 50  0000 C CNN
F 1 "D_Photo" H 5300 2554 50  0000 C CNN
F 2 "myParts:BPW_34_photodiode" H 5300 2350 50  0001 C CNN
F 3 "~" H 5300 2350 50  0001 C CNN
	1    5350 2350
	1    0    0    -1  
$EndComp
Text GLabel 5600 2200 2    50   Input ~ 0
pixel_7_B
Text GLabel 5600 2500 2    50   Input ~ 0
pixel_7_A
Text GLabel 5600 3200 2    50   Input ~ 0
pixel_5_A
Text GLabel 5600 2900 2    50   Input ~ 0
pixel_5_B
Text GLabel 5600 3650 2    50   Input ~ 0
pixel_3_B
Text GLabel 5600 3950 2    50   Input ~ 0
pixel_3_A
Text GLabel 5600 4650 2    50   Input ~ 0
pixel_1_A
Text GLabel 5600 4350 2    50   Input ~ 0
pixel_1_B
Wire Wire Line
	5450 2350 5600 2350
Wire Wire Line
	5600 2350 5600 2200
Wire Wire Line
	5600 2350 5600 2500
Connection ~ 5600 2350
Wire Wire Line
	5450 3050 5600 3050
Wire Wire Line
	5600 3050 5600 2900
Wire Wire Line
	5600 3050 5600 3200
Connection ~ 5600 3050
Wire Wire Line
	5450 3800 5600 3800
Wire Wire Line
	5600 3800 5600 3650
Wire Wire Line
	5600 3800 5600 3950
Connection ~ 5600 3800
Wire Wire Line
	5600 4350 5600 4500
Wire Wire Line
	5450 4500 5600 4500
Wire Wire Line
	5600 4500 5600 4650
Connection ~ 5600 4500
Text GLabel 3650 4350 0    50   Input ~ 0
pixel_2_B
Text GLabel 3650 4650 0    50   Input ~ 0
pixel_2_A
Text GLabel 3650 3650 0    50   Input ~ 0
pixel_4_B
Text GLabel 3650 3950 0    50   Input ~ 0
pixel_4_A
Text GLabel 3650 2900 0    50   Input ~ 0
pixel_6_B
Text GLabel 3650 3200 0    50   Input ~ 0
pixel_6_A
Text GLabel 3650 2200 0    50   Input ~ 0
pixel_8_B
Text GLabel 3650 2500 0    50   Input ~ 0
pixel_8_A
Wire Wire Line
	3650 2200 3650 2350
Wire Wire Line
	3650 2350 3800 2350
Wire Wire Line
	3650 2350 3650 2500
Connection ~ 3650 2350
Wire Wire Line
	3650 2900 3650 3050
Wire Wire Line
	3650 3050 3800 3050
Wire Wire Line
	3650 3050 3650 3200
Connection ~ 3650 3050
Wire Wire Line
	3650 3650 3650 3800
Wire Wire Line
	3650 3800 3800 3800
Wire Wire Line
	3650 3800 3650 3950
Connection ~ 3650 3800
Wire Wire Line
	3650 4350 3650 4500
Wire Wire Line
	3650 4500 3800 4500
Wire Wire Line
	3650 4500 3650 4650
Connection ~ 3650 4500
Wire Wire Line
	4100 2350 4650 2350
Wire Wire Line
	4100 3050 4650 3050
Wire Wire Line
	4100 3800 4650 3800
Wire Wire Line
	4100 4500 4650 4500
Wire Wire Line
	4650 2350 4650 3050
Connection ~ 4650 2350
Wire Wire Line
	4650 2350 5150 2350
Connection ~ 4650 4500
Wire Wire Line
	4650 4500 5150 4500
Connection ~ 4650 3800
Wire Wire Line
	4650 3800 5150 3800
Wire Wire Line
	4650 3800 4650 4500
Connection ~ 4650 3050
Wire Wire Line
	4650 3050 5150 3050
Wire Wire Line
	4650 3050 4650 3800
Text GLabel 4550 2150 0    50   Input ~ 0
TCO_A
Text GLabel 4750 4700 2    50   Input ~ 0
TCO_B
Wire Wire Line
	4750 4700 4650 4700
Wire Wire Line
	4650 4700 4650 4500
Wire Wire Line
	4550 2150 4650 2150
Wire Wire Line
	4650 2150 4650 2350
$EndSCHEMATC
