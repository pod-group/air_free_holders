EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J1
U 1 1 61A0B695
P 2950 2300
F 0 "J1" H 3000 1775 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 3000 1866 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x06_P2.54mm_Vertical" H 2950 2300 50  0001 C CNN
F 3 "~" H 2950 2300 50  0001 C CNN
	1    2950 2300
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J3
U 1 1 61A0D0DB
P 4850 2300
F 0 "J3" H 4900 1775 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 4900 1866 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x06_P2.54mm_Vertical" H 4850 2300 50  0001 C CNN
F 3 "~" H 4850 2300 50  0001 C CNN
	1    4850 2300
	1    0    0    1   
$EndComp
Text GLabel 4650 2200 0    50   Input ~ 0
TCO_SNS
Text GLabel 4650 2300 0    50   Input ~ 0
TCO_SRC
Text GLabel 3250 2300 2    50   Input ~ 0
TCO_SNS
Text GLabel 3250 2200 2    50   Input ~ 0
TCO_SRC
Text GLabel 3250 2100 2    50   Input ~ 0
pixel_8_SRC
Text GLabel 3250 2000 2    50   Input ~ 0
pixel_8_SNS
Text GLabel 4650 2100 0    50   Input ~ 0
pixel_7_SRC
Text GLabel 4650 2000 0    50   Input ~ 0
pixel_7_SNS
Text GLabel 2750 2000 0    50   Input ~ 0
pixel_6_SNS
Text GLabel 2750 2100 0    50   Input ~ 0
pixel_6_SRC
Text GLabel 2750 2300 0    50   Input ~ 0
pixel_4_SRC
Text GLabel 2750 2200 0    50   Input ~ 0
pixel_4_SNS
Text GLabel 2750 2400 0    50   Input ~ 0
pixel_2_SNS
Text GLabel 2750 2500 0    50   Input ~ 0
pixel_2_SRC
Text GLabel 5150 2500 2    50   Input ~ 0
pixel_1_SRC
Text GLabel 5150 2400 2    50   Input ~ 0
pixel_1_SNS
Text GLabel 5150 2300 2    50   Input ~ 0
pixel_3_SRC
Text GLabel 5150 2200 2    50   Input ~ 0
pixel_3_SNS
Text GLabel 5150 2100 2    50   Input ~ 0
pixel_5_SRC
Text GLabel 5150 2000 2    50   Input ~ 0
pixel_5_SNS
$Comp
L Connector:Conn_Coaxial J2
U 1 1 61A103AD
P 3450 800
F 0 "J2" H 3550 775 50  0000 L CNN
F 1 "Conn_Coaxial" H 3550 684 50  0000 L CNN
F 2 "myParts:90deg_BNC_radiall_R141665200" H 3450 800 50  0001 C CNN
F 3 " ~" H 3450 800 50  0001 C CNN
	1    3450 800 
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J4
U 1 1 61A11BEF
P 5000 800
F 0 "J4" H 5100 775 50  0000 L CNN
F 1 "Conn_Coaxial" H 5100 684 50  0000 L CNN
F 2 "myParts:90deg_BNC_radiall_R141665200" H 5000 800 50  0001 C CNN
F 3 " ~" H 5000 800 50  0001 C CNN
	1    5000 800 
	1    0    0    -1  
$EndComp
Text GLabel 3450 1000 3    50   Input ~ 0
TCO_SNS
Text GLabel 5000 1000 3    50   Input ~ 0
TCO_SRC
Text GLabel 3250 800  0    50   Input ~ 0
SNS_common
Text GLabel 4800 800  0    50   Input ~ 0
SRC_common
$Comp
L riede_ELQE_switchingBoard-rescue:ASSR-1611-001E-Relay_SolidState K1
U 1 1 61A1A116
P 2100 3450
F 0 "K1" H 2100 3775 50  0000 C CNN
F 1 "ASSR-1611-001E" H 2100 3684 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 2850 3550 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 2850 2650 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 2850 3350 50  0001 L CNN "Description"
F 5 "4.7" H 2850 3250 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 2850 3150 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 2850 2750 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 2850 2950 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 2850 2850 50  0001 L CNN "Manufacturer_Part_Number"
	1    2100 3450
	1    0    0    -1  
$EndComp
NoConn ~ 3250 2400
NoConn ~ 3250 2500
NoConn ~ 4650 2500
NoConn ~ 4650 2400
$Comp
L Connector_Generic:Conn_01x17 J5
U 1 1 61A1DF36
P 7950 1900
F 0 "J5" H 8030 1942 50  0000 L CNN
F 1 "Conn_01x17" H 8030 1851 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x17_P2.54mm_Vertical" H 7950 1900 50  0001 C CNN
F 3 "~" H 7950 1900 50  0001 C CNN
	1    7950 1900
	1    0    0    1   
$EndComp
Text GLabel 9000 1100 2    50   Input ~ 0
Arduino_D15
Text GLabel 9000 1200 2    50   Input ~ 0
Arduino_D14
Text GLabel 9000 1300 2    50   Input ~ 0
Arduino_V_in
Text GLabel 9000 1400 2    50   Input ~ 0
Arduino_GND
Text GLabel 9000 1500 2    50   Input ~ 0
Arduino_reset
Text GLabel 9000 1600 2    50   Input ~ 0
Arduino_5V
Text GLabel 9000 1900 2    50   Input ~ 0
Arduino_D23
Text GLabel 9000 2000 2    50   Input ~ 0
Arduino_D22
Text GLabel 9000 2100 2    50   Input ~ 0
Arduino_D21
Text GLabel 9000 2200 2    50   Input ~ 0
Arduino_D20
Text GLabel 9000 2300 2    50   Input ~ 0
Arduino_D19
Text GLabel 9000 2400 2    50   Input ~ 0
Arduino_D18
Text GLabel 9000 2700 2    50   Input ~ 0
Arduino_D13
Text GLabel 9000 2500 2    50   Input ~ 0
AREF
Text GLabel 9000 2600 2    50   Input ~ 0
3V3
Text GLabel 7750 1100 0    50   Input ~ 0
Arduino_D16
Text GLabel 7750 1200 0    50   Input ~ 0
Arduino_D17
Text GLabel 7750 1300 0    50   Input ~ 0
Arduino_D1
Text GLabel 7750 1400 0    50   Input ~ 0
Arduino_D0
Text GLabel 7750 1500 0    50   Input ~ 0
Arduino_reset
Text GLabel 7750 1600 0    50   Input ~ 0
Arduino_GND
Text GLabel 7750 1700 0    50   Input ~ 0
Arduino_D2
Text GLabel 7750 1800 0    50   Input ~ 0
Arduino_D3
Text GLabel 7750 1900 0    50   Input ~ 0
Arduino_D4
Text GLabel 7750 2000 0    50   Input ~ 0
Arduino_D5
Text GLabel 7750 2100 0    50   Input ~ 0
Arduino_D6
Text GLabel 7750 2200 0    50   Input ~ 0
Arduino_D7
Text GLabel 7750 2300 0    50   Input ~ 0
Arduino_D8
Text GLabel 7750 2400 0    50   Input ~ 0
Arduino_D9
Text GLabel 7750 2500 0    50   Input ~ 0
Arduino_D10
Text GLabel 7750 2600 0    50   Input ~ 0
Arduino_D11
Text GLabel 7750 2700 0    50   Input ~ 0
Arduino_D12
NoConn ~ 9000 1700
NoConn ~ 9000 1800
Text GLabel 6850 1200 0    50   Input ~ 0
Arduino_D17
Text GLabel 10100 1300 2    50   Input ~ 0
Arduino_V_in
Text GLabel 10100 1600 2    50   Input ~ 0
Arduino_5V
Text GLabel 10100 1900 2    50   Input ~ 0
Arduino_D23
Text GLabel 10100 2000 2    50   Input ~ 0
Arduino_D22
Text GLabel 10100 2100 2    50   Input ~ 0
Arduino_D21
Text GLabel 10100 2200 2    50   Input ~ 0
Arduino_D20
Text GLabel 10100 2300 2    50   Input ~ 0
Arduino_D19
Text GLabel 10100 2400 2    50   Input ~ 0
Arduino_D18
$Comp
L Connector_Generic:Conn_01x02 J7
U 1 1 61A27FB1
P 9900 3500
F 0 "J7" H 9818 3175 50  0000 C CNN
F 1 "Conn_01x02" H 9818 3266 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9900 3500 50  0001 C CNN
F 3 "~" H 9900 3500 50  0001 C CNN
	1    9900 3500
	-1   0    0    1   
$EndComp
Text GLabel 10100 3400 2    50   Input ~ 0
Arduino_reset
Text GLabel 10100 3500 2    50   Input ~ 0
Arduino_GND
Text GLabel 7450 3400 0    50   Input ~ 0
SSR_currLimResistor
$Comp
L Device:R R1
U 1 1 61A29FB8
P 7600 3400
F 0 "R1" V 7393 3400 50  0000 C CNN
F 1 "470R" V 7484 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_2512_6332Metric_Pad1.52x3.35mm_HandSolder" V 7530 3400 50  0001 C CNN
F 3 "~" H 7600 3400 50  0001 C CNN
	1    7600 3400
	0    1    1    0   
$EndComp
Text GLabel 7750 3400 2    50   Input ~ 0
Arduino_GND
Text GLabel 1800 3450 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 2400 3350 2    50   Input ~ 0
SNS_common
$Comp
L riede_ELQE_switchingBoard-rescue:ASSR-1611-001E-Relay_SolidState K2
U 1 1 61A2DB43
P 2100 4000
F 0 "K2" H 2100 4325 50  0000 C CNN
F 1 "ASSR-1611-001E" H 2100 4234 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 2850 4100 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 2850 3200 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 2850 3900 50  0001 L CNN "Description"
F 5 "4.7" H 2850 3800 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 2850 3700 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 2850 3300 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 2850 3500 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 2850 3400 50  0001 L CNN "Manufacturer_Part_Number"
	1    2100 4000
	1    0    0    -1  
$EndComp
Text GLabel 1800 4000 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 2400 3900 2    50   Input ~ 0
SRC_common
$Comp
L riede_ELQE_switchingBoard-rescue:ASSR-1611-001E-Relay_SolidState K3
U 1 1 61A34D1A
P 2100 4550
F 0 "K3" H 2100 4875 50  0000 C CNN
F 1 "ASSR-1611-001E" H 2100 4784 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 2850 4650 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 2850 3750 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 2850 4450 50  0001 L CNN "Description"
F 5 "4.7" H 2850 4350 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 2850 4250 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 2850 3850 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 2850 4050 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 2850 3950 50  0001 L CNN "Manufacturer_Part_Number"
	1    2100 4550
	1    0    0    -1  
$EndComp
Text GLabel 1800 4550 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 2400 4450 2    50   Input ~ 0
SNS_common
$Comp
L riede_ELQE_switchingBoard-rescue:ASSR-1611-001E-Relay_SolidState K4
U 1 1 61A34D28
P 2100 5100
F 0 "K4" H 2100 5425 50  0000 C CNN
F 1 "ASSR-1611-001E" H 2100 5334 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 2850 5200 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 2850 4300 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 2850 5000 50  0001 L CNN "Description"
F 5 "4.7" H 2850 4900 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 2850 4800 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 2850 4400 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 2850 4600 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 2850 4500 50  0001 L CNN "Manufacturer_Part_Number"
	1    2100 5100
	1    0    0    -1  
$EndComp
Text GLabel 1800 5100 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 2400 5000 2    50   Input ~ 0
SRC_common
$Comp
L riede_ELQE_switchingBoard-rescue:ASSR-1611-001E-Relay_SolidState K5
U 1 1 61A3BAEC
P 2100 5650
F 0 "K5" H 2100 5975 50  0000 C CNN
F 1 "ASSR-1611-001E" H 2100 5884 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 2850 5750 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 2850 4850 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 2850 5550 50  0001 L CNN "Description"
F 5 "4.7" H 2850 5450 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 2850 5350 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 2850 4950 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 2850 5150 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 2850 5050 50  0001 L CNN "Manufacturer_Part_Number"
	1    2100 5650
	1    0    0    -1  
$EndComp
Text GLabel 1800 5650 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 2400 5550 2    50   Input ~ 0
SNS_common
$Comp
L riede_ELQE_switchingBoard-rescue:ASSR-1611-001E-Relay_SolidState K6
U 1 1 61A3BAFA
P 2100 6200
F 0 "K6" H 2100 6525 50  0000 C CNN
F 1 "ASSR-1611-001E" H 2100 6434 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 2850 6300 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 2850 5400 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 2850 6100 50  0001 L CNN "Description"
F 5 "4.7" H 2850 6000 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 2850 5900 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 2850 5500 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 2850 5700 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 2850 5600 50  0001 L CNN "Manufacturer_Part_Number"
	1    2100 6200
	1    0    0    -1  
$EndComp
Text GLabel 1800 6200 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 2400 6100 2    50   Input ~ 0
SRC_common
$Comp
L riede_ELQE_switchingBoard-rescue:ASSR-1611-001E-Relay_SolidState K7
U 1 1 61A3BB08
P 2100 6750
F 0 "K7" H 2100 7075 50  0000 C CNN
F 1 "ASSR-1611-001E" H 2100 6984 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 2850 6850 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 2850 5950 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 2850 6650 50  0001 L CNN "Description"
F 5 "4.7" H 2850 6550 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 2850 6450 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 2850 6050 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 2850 6250 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 2850 6150 50  0001 L CNN "Manufacturer_Part_Number"
	1    2100 6750
	1    0    0    -1  
$EndComp
Text GLabel 1800 6750 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 2400 6650 2    50   Input ~ 0
SNS_common
$Comp
L riede_ELQE_switchingBoard-rescue:ASSR-1611-001E-Relay_SolidState K8
U 1 1 61A3BB16
P 2100 7300
F 0 "K8" H 2100 7625 50  0000 C CNN
F 1 "ASSR-1611-001E" H 2100 7534 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 2850 7400 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 2850 6500 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 2850 7200 50  0001 L CNN "Description"
F 5 "4.7" H 2850 7100 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 2850 7000 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 2850 6600 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 2850 6800 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 2850 6700 50  0001 L CNN "Manufacturer_Part_Number"
	1    2100 7300
	1    0    0    -1  
$EndComp
Text GLabel 1800 7300 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 2400 7200 2    50   Input ~ 0
SRC_common
$Comp
L riede_ELQE_switchingBoard-rescue:ASSR-1611-001E-Relay_SolidState K9
U 1 1 61A83096
P 4300 3450
F 0 "K9" H 4300 3775 50  0000 C CNN
F 1 "ASSR-1611-001E" H 4300 3684 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 5050 3550 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 5050 2650 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 5050 3350 50  0001 L CNN "Description"
F 5 "4.7" H 5050 3250 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 5050 3150 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 5050 2750 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 5050 2950 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 5050 2850 50  0001 L CNN "Manufacturer_Part_Number"
	1    4300 3450
	1    0    0    -1  
$EndComp
Text GLabel 4000 3450 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 4600 3350 2    50   Input ~ 0
SNS_common
$Comp
L riede_ELQE_switchingBoard-rescue:ASSR-1611-001E-Relay_SolidState K10
U 1 1 61A830A4
P 4300 4000
F 0 "K10" H 4300 4325 50  0000 C CNN
F 1 "ASSR-1611-001E" H 4300 4234 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 5050 4100 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 5050 3200 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 5050 3900 50  0001 L CNN "Description"
F 5 "4.7" H 5050 3800 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 5050 3700 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 5050 3300 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 5050 3500 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 5050 3400 50  0001 L CNN "Manufacturer_Part_Number"
	1    4300 4000
	1    0    0    -1  
$EndComp
Text GLabel 4000 4000 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 4600 3900 2    50   Input ~ 0
SRC_common
$Comp
L riede_ELQE_switchingBoard-rescue:ASSR-1611-001E-Relay_SolidState K11
U 1 1 61A830B2
P 4300 4550
F 0 "K11" H 4300 4875 50  0000 C CNN
F 1 "ASSR-1611-001E" H 4300 4784 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 5050 4650 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 5050 3750 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 5050 4450 50  0001 L CNN "Description"
F 5 "4.7" H 5050 4350 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 5050 4250 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 5050 3850 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 5050 4050 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 5050 3950 50  0001 L CNN "Manufacturer_Part_Number"
	1    4300 4550
	1    0    0    -1  
$EndComp
Text GLabel 4000 4550 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 4600 4450 2    50   Input ~ 0
SNS_common
$Comp
L riede_ELQE_switchingBoard-rescue:ASSR-1611-001E-Relay_SolidState K12
U 1 1 61A830C0
P 4300 5100
F 0 "K12" H 4300 5425 50  0000 C CNN
F 1 "ASSR-1611-001E" H 4300 5334 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 5050 5200 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 5050 4300 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 5050 5000 50  0001 L CNN "Description"
F 5 "4.7" H 5050 4900 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 5050 4800 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 5050 4400 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 5050 4600 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 5050 4500 50  0001 L CNN "Manufacturer_Part_Number"
	1    4300 5100
	1    0    0    -1  
$EndComp
Text GLabel 4000 5100 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 4600 5000 2    50   Input ~ 0
SRC_common
$Comp
L riede_ELQE_switchingBoard-rescue:ASSR-1611-001E-Relay_SolidState K13
U 1 1 61A830CE
P 4300 5650
F 0 "K13" H 4300 5975 50  0000 C CNN
F 1 "ASSR-1611-001E" H 4300 5884 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 5050 5750 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 5050 4850 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 5050 5550 50  0001 L CNN "Description"
F 5 "4.7" H 5050 5450 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 5050 5350 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 5050 4950 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 5050 5150 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 5050 5050 50  0001 L CNN "Manufacturer_Part_Number"
	1    4300 5650
	1    0    0    -1  
$EndComp
Text GLabel 4000 5650 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 4600 5550 2    50   Input ~ 0
SNS_common
$Comp
L riede_ELQE_switchingBoard-rescue:ASSR-1611-001E-Relay_SolidState K14
U 1 1 61A830DC
P 4300 6200
F 0 "K14" H 4300 6525 50  0000 C CNN
F 1 "ASSR-1611-001E" H 4300 6434 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 5050 6300 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 5050 5400 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 5050 6100 50  0001 L CNN "Description"
F 5 "4.7" H 5050 6000 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 5050 5900 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 5050 5500 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 5050 5700 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 5050 5600 50  0001 L CNN "Manufacturer_Part_Number"
	1    4300 6200
	1    0    0    -1  
$EndComp
Text GLabel 4000 6200 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 4600 6100 2    50   Input ~ 0
SRC_common
$Comp
L riede_ELQE_switchingBoard-rescue:ASSR-1611-001E-Relay_SolidState K15
U 1 1 61A830EA
P 4300 6750
F 0 "K15" H 4300 7075 50  0000 C CNN
F 1 "ASSR-1611-001E" H 4300 6984 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 5050 6850 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 5050 5950 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 5050 6650 50  0001 L CNN "Description"
F 5 "4.7" H 5050 6550 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 5050 6450 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 5050 6050 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 5050 6250 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 5050 6150 50  0001 L CNN "Manufacturer_Part_Number"
	1    4300 6750
	1    0    0    -1  
$EndComp
Text GLabel 4000 6750 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 4600 6650 2    50   Input ~ 0
SNS_common
$Comp
L riede_ELQE_switchingBoard-rescue:ASSR-1611-001E-Relay_SolidState K16
U 1 1 61A830F8
P 4300 7300
F 0 "K16" H 4300 7625 50  0000 C CNN
F 1 "ASSR-1611-001E" H 4300 7534 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 5050 7400 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 5050 6500 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 5050 7200 50  0001 L CNN "Description"
F 5 "4.7" H 5050 7100 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 5050 7000 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 5050 6600 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 5050 6800 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 5050 6700 50  0001 L CNN "Manufacturer_Part_Number"
	1    4300 7300
	1    0    0    -1  
$EndComp
Text GLabel 4000 7300 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 4600 7200 2    50   Input ~ 0
SRC_common
Text GLabel 2400 3550 2    50   Input ~ 0
pixel_8_SNS
Text GLabel 2400 4100 2    50   Input ~ 0
pixel_8_SRC
Text GLabel 2400 4650 2    50   Input ~ 0
pixel_6_SNS
Text GLabel 2400 5200 2    50   Input ~ 0
pixel_6_SRC
Text GLabel 2400 5750 2    50   Input ~ 0
pixel_4_SNS
Text GLabel 2400 6300 2    50   Input ~ 0
pixel_4_SRC
Text GLabel 2400 6850 2    50   Input ~ 0
pixel_2_SNS
Text GLabel 2400 7400 2    50   Input ~ 0
pixel_2_SRC
Text GLabel 4600 7400 2    50   Input ~ 0
pixel_1_SRC
Text GLabel 4600 6850 2    50   Input ~ 0
pixel_1_SNS
Text GLabel 4600 6300 2    50   Input ~ 0
pixel_3_SRC
Text GLabel 4600 5750 2    50   Input ~ 0
pixel_3_SNS
Text GLabel 4600 5200 2    50   Input ~ 0
pixel_5_SRC
Text GLabel 4600 4650 2    50   Input ~ 0
pixel_5_SNS
Text GLabel 4600 4100 2    50   Input ~ 0
pixel_7_SRC
Text GLabel 4600 3550 2    50   Input ~ 0
pixel_7_SNS
Text GLabel 4000 7200 0    50   Input ~ 0
Arduino_D0
Text GLabel 4000 6650 0    50   Input ~ 0
Arduino_D1
Text GLabel 1800 7200 0    50   Input ~ 0
Arduino_D2
Text GLabel 1800 6650 0    50   Input ~ 0
Arduino_D3
Text GLabel 4000 6100 0    50   Input ~ 0
Arduino_D4
Text GLabel 4000 5550 0    50   Input ~ 0
Arduino_D5
Text GLabel 1800 6100 0    50   Input ~ 0
Arduino_D6
Text GLabel 1800 5550 0    50   Input ~ 0
Arduino_D7
Text GLabel 4000 5000 0    50   Input ~ 0
Arduino_D8
Text GLabel 4000 4450 0    50   Input ~ 0
Arduino_D9
Text GLabel 1800 5000 0    50   Input ~ 0
Arduino_D10
Text GLabel 1800 4450 0    50   Input ~ 0
Arduino_D11
Text GLabel 4000 3900 0    50   Input ~ 0
Arduino_D12
Text GLabel 1800 3350 0    50   Input ~ 0
Arduino_D15
Text GLabel 4000 3350 0    50   Input ~ 0
Arduino_D13
Text GLabel 1800 3900 0    50   Input ~ 0
Arduino_D14
NoConn ~ 10100 1300
NoConn ~ 10100 1600
NoConn ~ 10100 1900
NoConn ~ 10100 2000
NoConn ~ 10100 2100
NoConn ~ 10100 2200
NoConn ~ 10100 2300
NoConn ~ 10100 2400
NoConn ~ 6850 1200
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 61A99EB8
P 7450 4000
F 0 "H1" V 7404 4150 50  0000 L CNN
F 1 "MountingHole_Pad" V 7495 4150 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 7450 4000 50  0001 C CNN
F 3 "~" H 7450 4000 50  0001 C CNN
	1    7450 4000
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 61A9AA10
P 7450 4200
F 0 "H2" V 7404 4350 50  0000 L CNN
F 1 "MountingHole_Pad" V 7495 4350 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 7450 4200 50  0001 C CNN
F 3 "~" H 7450 4200 50  0001 C CNN
	1    7450 4200
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 61A9B42E
P 7450 4400
F 0 "H3" V 7404 4550 50  0000 L CNN
F 1 "MountingHole_Pad" V 7495 4550 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 7450 4400 50  0001 C CNN
F 3 "~" H 7450 4400 50  0001 C CNN
	1    7450 4400
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 61A9B73F
P 7450 4600
F 0 "H4" V 7404 4750 50  0000 L CNN
F 1 "MountingHole_Pad" V 7495 4750 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 7450 4600 50  0001 C CNN
F 3 "~" H 7450 4600 50  0001 C CNN
	1    7450 4600
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H5
U 1 1 61A9B9CB
P 7450 4800
F 0 "H5" V 7404 4950 50  0000 L CNN
F 1 "MountingHole_Pad" V 7495 4950 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 7450 4800 50  0001 C CNN
F 3 "~" H 7450 4800 50  0001 C CNN
	1    7450 4800
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H6
U 1 1 61A9BD3B
P 7450 5000
F 0 "H6" V 7404 5150 50  0000 L CNN
F 1 "MountingHole_Pad" V 7495 5150 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 7450 5000 50  0001 C CNN
F 3 "~" H 7450 5000 50  0001 C CNN
	1    7450 5000
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H7
U 1 1 61A9C098
P 7450 5200
F 0 "H7" V 7404 5350 50  0000 L CNN
F 1 "MountingHole_Pad" V 7495 5350 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 7450 5200 50  0001 C CNN
F 3 "~" H 7450 5200 50  0001 C CNN
	1    7450 5200
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H8
U 1 1 61A9C512
P 7450 5400
F 0 "H8" V 7404 5550 50  0000 L CNN
F 1 "MountingHole_Pad" V 7495 5550 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 7450 5400 50  0001 C CNN
F 3 "~" H 7450 5400 50  0001 C CNN
	1    7450 5400
	0    1    1    0   
$EndComp
Text GLabel 7350 4000 0    50   Input ~ 0
Arduino_GND
Text GLabel 7350 4200 0    50   Input ~ 0
Arduino_GND
Text GLabel 7350 4400 0    50   Input ~ 0
Arduino_GND
Text GLabel 7350 4600 0    50   Input ~ 0
Arduino_GND
Text GLabel 7350 4800 0    50   Input ~ 0
Arduino_GND
Text GLabel 7350 5000 0    50   Input ~ 0
Arduino_GND
Text GLabel 7350 5200 0    50   Input ~ 0
Arduino_GND
Text GLabel 7350 5400 0    50   Input ~ 0
Arduino_GND
NoConn ~ 2400 3450
NoConn ~ 2400 4000
NoConn ~ 2400 4550
NoConn ~ 2400 5100
NoConn ~ 2400 5650
NoConn ~ 2400 6200
NoConn ~ 2400 6750
NoConn ~ 2400 7300
NoConn ~ 4600 7300
NoConn ~ 4600 6750
NoConn ~ 4600 6200
NoConn ~ 4600 5650
NoConn ~ 4600 5100
NoConn ~ 4600 4550
NoConn ~ 4600 4000
NoConn ~ 4600 3450
Text GLabel 6850 1100 0    50   Input ~ 0
Arduino_D16
NoConn ~ 6850 1100
Text GLabel 10100 2500 2    50   Input ~ 0
AREF
Text GLabel 10100 2600 2    50   Input ~ 0
3V3
NoConn ~ 10100 2600
NoConn ~ 10100 2500
$Comp
L Connector_Generic:Conn_01x17 J6
U 1 1 61A201AE
P 8800 1900
F 0 "J6" H 8718 2917 50  0000 C CNN
F 1 "Conn_01x17" H 8718 2826 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x17_P2.54mm_Vertical" H 8800 1900 50  0001 C CNN
F 3 "~" H 8800 1900 50  0001 C CNN
	1    8800 1900
	-1   0    0    1   
$EndComp
$EndSCHEMATC
