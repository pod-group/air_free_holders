from serial import Serial
import serial.tools.list_ports



import tkinter as tk
from tkinter import *
from tkinter import ttk
from tkinter.ttk import *
from tkinter import Menu
from tkinter import messagebox
import array

import numpy as np

#from Tkinter import *



import multiprocessing
import threading
import logging

import re

from datetime import date
import time
import os
import shutil
import csv

#initialise and define global values
actionBrackets = ['<' , '>']
queryBrackets = ['?' , '?']
panicBrackets = ['!' , '!']
startBrackets = [actionBrackets[0] , queryBrackets[0] , panicBrackets[0]]
endBrackets = [actionBrackets[1] , queryBrackets[1] , panicBrackets[1]]



class cmd:
    def __init__(self):
        self.commandLength = 12
        self.cmdString = ""
        self.brackets = [actionBrackets , queryBrackets , panicBrackets]
        self.commandBrackets = 0
        self.queryBrackets = 1
        self.panicBrackets = 2
        self.commandString = ""
        self.started = 0
        self.finished = 0
        self.type = 0
        self.subsystem = 0
        self.location = 0
        self.number = 0
        self.mode = 0
        self.isValid = 0
        self.timeIn = 0
        self.timeOut = 0
        self.elapsedTime = 0
        self.returnValue = 0
        self.returnString = 0

class pixel:
    def __init__(self, pixelNumber):
        self.number = pixelNumber
        self.measurementMode = 0
        self.currentValue = 0
        self.outputPin = 0 #might change arduino firmware to switch individual pins based on inputs, not sure if that is best way as pins would have to be defined unless bit bashed each time - not sure if efficient


    def enablePixel(self , whichArduino):
        #generates command to be sent to arduino for current state of pixel and current measurement mode - i.e. 2wire or 4wire.
        pixelCommand = cmd()
        pixelCommand.brackets = pixelCommand.actionBrackets
        pixelCommand.sybsystem = 'pxl'
        pixelCommand.location = self.number
        pixelCommand.mode = self.measurementMode

        pixelCommand.commandString = pixelCommand.brackets[0] + pixelCommand.subsystem + '_' + pixelCommand.location + '_' + pixelCommand.mode + pixelCommand.brackets[1]
        pixelCommand.timeOut = time.time()
        print(pixelCommand.commandString)
        whichArduino.send(pixelCommand.commandString)
        time.sleep(0.001)
        pixelCommand.returnString = whichArduino.receive()
        print(pixelCommand.returnString)
        pixelCommand.timeIn = time.time()
        pixelCommand.elapsedTime = pixelCommand.timeIn - pixelCommand.timeOut
        print(pixelCommand.elapsedTime)
        tempString = pixelCommand.brackets[0] + '|_|' + pixelCommand.brackets[1]
        returnedSegments = re.split(tempString)


class muxSystem:
    def __init__(self):
        self.numPixels = 0
        self.numArduinos = 0
        self.availableSerialports = []
        self.availableModes = 0
        self.pixels = []
        self.selectedMode = 0
        self.selectedPixel = 0

        i = 0
        while(i < self.numPixels):
            self.pixels[i] = pixel(i)
        self.serialPort = []
        self.arduinos = []
        self.arduinos.append(ArduinoObject(0 , self.serialPort , 2000000))
        self.command = cmd()

    def updateAvailableModes(self):
        #handshake with arduino, arduino tells snake lad whether 2 or 4 wire modes are allowed
        pass

    def updatePixels(self):
        j = 0
        while(j < self.numPixels):
            self.pixels[j] = pixel(j)

    def updateSerialPort(self , inputSerialPort):
        self.serialPort = inputSerialPort
        self.arduinos[0] = ArduinoObject(0 , self.serialPort , 20000000)



def checkAvailableSerial():
    #Ask the snake to look for available COM ports, return these as an array of objects
    ports = serial.tools.list_ports.comports(include_links=False)
    #create lists for later on, not yet sure exactly which of these will be needed later on, but easy to have and memory is cheap
    portsList = []
    portsDescriptionList = []
    portsMenuList = []
    outputArray = []
    #iterate through and generate lists that will be used later on, print statements for debugging and testing, remove later if necessary
    for port in ports:
        # print(port.device)
        portsList.append(port.device)
        # print(port.description)
        portsDescriptionList.append(port.description)
        portsMenuList.append(port.device + ' - ' + port.description)

    #print(portsList)
    #print(portsDescriptionList)
    #print(portsMenuList)
    #generate 2D output array to get passed to main GUI function
    outputArray.append(portsList)
    outputArray.append(portsDescriptionList)
    outputArray.append(portsMenuList)
    #print(outputArray)
    return(outputArray)

class ArduinoObject:
    def __init__(self, number, tempPort , tempBaudrate):
        tempPortString = str(tempPort)
        self.number = number    #might want to add multiple arduinos at some point. have plenty of available memory to have this variable.
        self.comms = serial.Serial()
        self.comms.port = tempPortString
        self.comms.baudrate = tempBaudrate
        self.isInitialised = 0
        self.initialisationFailed = 0
        self.numPixels = 0
        self.measurementMode = 0


    def initialise(self):
        self.comms.open()
        print('Serial Port Open')
        butts = 'butts'
        #print(butts)
        self.comms.write(butts.encode())
        time.sleep(0.1)
        #initialises Serial connection with Arduino - call this function at login - include some error handling in case the arduino does not connect properly. If multiple arduinos in system then search for serial connections, ping each one with a query in succession until right one is found.
        #need to figure out initialisation protocol. Maybe just send it a hello and the arduino responds
        #Need a time-out clause to throw an error for when arduino shits the bed
        firstHandshake = self.receiveAll()
        print(firstHandshake)
        print('First Contact Made')#first handshake done, should print <START> to console
        self.comms.write(butts.encode())
        time.sleep(0.5)
        secondHandshake = self.receiveAll()
        #print(secondHandshake)
        #decode second handshake to parse numPixels and allowed measurement modes
        #if handshake sucessful

        if(secondHandshake == ''):
            self.initialisationFailed = 1
            self.isInitialised = 0
        else:
            messageToDecode_temp = secondHandshake.split('_')
            messageToDecode = messageToDecode_temp
            tempLength = len(messageToDecode)
            if(tempLength > 1):
                messageToDecode[0] = messageToDecode_temp[0].lstrip('<')
                messageToDecode[1] = messageToDecode_temp[1]
                messageToDecode[2] = messageToDecode_temp[2].rstrip('>\r\n')# must be .rstrip >\r\n because of issue with string.split. Does not work without this part,
                #print(messageToDecode_temp)
                #print(messageToDecode)
                self.numPixels = int(messageToDecode[1])
                self.measurementMode = int(messageToDecode[2])
                if(self.numPixels == 6 or self.numPixels == 8):
                    if(self.measurementMode == 2 or self.measurementMode == 4):
                        self.isInitialised = 1
                        self.initialisationFailed = 0
                        #print(self.numPixels)
                        #print(self.measurementMode)
                else:
                    self.isInitialised = 0
                    self.initialisationFailed = 1
            else:
                self.isInitialised = 0
                self.initialisationFailed = 1

            #print(self.isInitialised)
            #print(self.initialisationFailed)


        #else if handshake fails
        #self.initialisationFailed = 1
        #self.isInitialised = 0
        if(self.isInitialised == 1):
            print('Handshake Complete!')
        else:
            print('Initialisation Failed :(')
        # wait for confirmation that arduino fully initialised
        # Add more error handling in here once I flesh out the initialisation process on the arduino.

    def waitFor(self):
        response = ""
        self.send("Can I please start?")
        time.sleep(1)
        response = self.receiveAll()
        print(response)
        time.sleep(1)
        response = self.receiveAll()

        # while (response.find("<START>") == -1):  # looks for initialisation finishing part. Might have to skip some of the SD shenanigans in the arduino script
        #     while (self.comms.inWaiting() == 0):
        #          pass  # loops without doing anything while nothing in serial buffer
        #     response += self.receiveAll()

        print("Response:" + str(response) + "\n")

    def send(self , tempCommand):
        self.comms.write(tempCommand.encode())

    def promptSend(self):
        tempCommand = input("Enter Command , syntax <XXX_YYY_ZZZ>, brackets <> , ?? , !!")
        self.comms.write(tempCommand.encode())

    def receiveAll(self):
        # function for reading in data from Serial port. Eventually will add on to this some extra functionality that will look for start and end markers
        # for initial testing will just read everything from serial connection - arduino will parse command and then respond by printing EVERYTHING to the serial connection. (also to file)
        receivedMessage = ""
        tempBrackets = ["", ""]
        temp = "="
        tempStarted = 0
        tempFinished = 0
        byteCount = -1  # tutorial has this, not entirely sure what it does. Might be needed for something later on. Needs to be -1 so that the array length matches later?
        maxTimeoutWaitSeconds = 3
        timeoutStepDelay = 0.001
        timeoutSteps = maxTimeoutWaitSeconds/timeoutStepDelay
        timeoutStepCounter = 0
        while (self.comms.inWaiting() == 0 and timeoutStepCounter < timeoutSteps):
            time.sleep(timeoutStepDelay)
            timeoutStepCounter+=1

        while (self.comms.inWaiting() > 0):
            temp = self.comms.read().decode()
            receivedMessage = receivedMessage + temp
            byteCount += 1
            if (byteCount == 0):
                time.sleep(0.002)  # delay after first character to make sure that everything hits serial buffer, can ultimately increase this to 1ms if I want, but not probably not necessary as
            # less will be hitting serial buffer in actual script. Only here for testing. If delay is 300µs then things dont work.

        return receivedMessage

    def receive(self):
        # function for reading in data from Serial port. Eventually will add on to this some extra functionality that will look for start and end markers
        # for initial testing will just read everything from serial connection - arduino will parse command and then respond by printing EVERYTHING to the serial connection. (also to file)
        receivedMessage = ""
        tempBrackets = ["", ""]
        temp = "="
        tempStarted = 0
        tempFinished = 0
        byteCount = -1  # tutorial has this, not entirely sure what it does. Might be needed for something later on. Needs to be -1 so that the array length matches later?
        while (tempFinished == 0):
            while (self.comms.inWaiting() == 0):
                pass
            while (self.comms.inWaiting() > 0 and tempFinished == 0):
                temp = self.comms.read().decode()
                if (temp in startBrackets and tempStarted == 0):  # recognises first character of response. Rest of loop will continue until the end character is read.
                    tempStarted = 1
                    tempBrackets[0] = temp
                    receivedMessage = receivedMessage + temp
                elif (tempStarted == 1):  # else if required here. Will skip this statement if condition to above if() statement is true.
                    receivedMessage = receivedMessage + temp
                    if (temp in endBrackets):
                        tempFinished = 1
                        tempBrackets[1] = temp
                byteCount += 1
                # debugging - check whats actually being read in. Will print tempFinished = 1 a couple of times due to termination of string.
                # print(tempFinished)
                # print(temp)
                # if(byteCount == 0):
                #    time.sleep(0.002)#delay after first character to make sure that everything hits serial buffer, can ultimately increase this to 1ms if I want, but not probably not necessary as
                # less will be hitting serial buffer in actual script. Only here for testing. If delay is 300µs then things dont work.
            #print(receivedMessage)
        return receivedMessage

class GUI_window:
    def __init__(self, tempMuxSystem: muxSystem):
        self.muxSystem = tempMuxSystem
        self.master = Tk()
        self.frame = tk.Frame(self.master)
        self.frame.grid()
        self.master.title = "Main Window Title"
        self.tab_definition = ttk.Notebook(self.frame)
        self.tab_setup = ttk.Frame(self.tab_definition)
        self.tab_control = ttk.Frame(self.tab_definition)
        self.pixelsMenu = [0]
        self.modeMenu = [0]
        self.printCorrespondenceToTerminal = IntVar(self.master , 0)

        if(self.muxSystem.arduinos[0].numPixels == 6):
            self.pixelsMenu = [1 , 2 , 3 , 4, 5 , 6] #
        elif(self.muxSystem.arduinos[0].numPixels == 8):
            self.pixelsMenu = [1 , 2 , 3 , 4 , 5 , 6 , 7 , 8]
        else:
            self.pixelsMenu = [0]

        if(self.muxSystem.arduinos[0].measurementMode == 4):
            self.modeMenu = [2 , 4]
        elif(self.muxSystem.arduinos[0].measurementMode == 2):
            self.modeMenu = [2]
        else:
            self.modeMenu = [0]

        self.tab_definition.add(self.tab_setup, text="Setup")
        self.tab_definition.add(self.tab_control, text="Mux Control")

        self.tab_definition.pack(expand=1, fill="both")

        #self.configureGUI()
        #self.runProgram()

        # Function below works as intended - TODO: arduino.initialise() add in handshake, arduino.readALL() add timeout clause
        def selectSerialPort(self):
            # gui setup looks for available serial ports
            # menu shows strings with descriptions, remove descriptions here to get serial port.
            tempString = menu_availableSerialPorts.get()
            tempPorts = tempString.split(' - ')
            self.muxSystem.updateSerialPort(tempPorts[0])
            #print(tempString)
            print(self.muxSystem.serialPort)
            self.muxSystem.arduinos[0].initialise()
            if (self.muxSystem.arduinos[0].isInitialised == 1 and self.muxSystem.arduinos[0].initialisationFailed == 0):
                messagebox.showinfo('Error', 'Initialisation Successful! \n System functional, move to Mux Control tab :)')

                btn_updatePixelMode['state'] = tk.NORMAL
                btn_idleState['state'] = tk.NORMAL
                btn_PANIC['state'] = tk.NORMAL
                #btn_yeet['state'] = tk.NORMAL
                #btn_yolo['state'] = tk.NORMAL

                if (self.muxSystem.arduinos[0].numPixels == 6):
                    self.pixelsMenu = [1, 2, 3, 4, 5, 6]
                elif (self.muxSystem.arduinos[0].numPixels == 8):
                    self.pixelsMenu = [1, 2, 3, 4, 5, 6, 7, 8]
                else:
                    self.pixelsMenu = [0]

                if (self.muxSystem.arduinos[0].measurementMode == 4):
                    self.modeMenu = [2, 4]
                elif (self.muxSystem.arduinos[0].measurementMode == 2):
                    self.modeMenu = [2]
                else:
                    self.modeMenu = [0]


                menu_selectPixel['values'] = self.pixelsMenu
                menu_selectMode['values'] = self.modeMenu


            elif (self.muxSystem.arduinos[0].isInitialised == 0 and self.muxSystem.arduinos[0].initialisationFailed == 1):
                messagebox.showinfo('Error', 'Task Failed\n Try resetting the arduino\n Confirm Arduino Serial Port')
            else:
                pass

        # Function below works as intended.
        def searchForAvailableSerial(self):
            # button runs function looking for available serial ports, generates list of available serial for drop down menu later
            # if wrong serial port is selected/wrong response is given then will throw and catch error. Will not allow more data to transmit other than handshake signal.
            self.muxSystem.availableSerialports = checkAvailableSerial()
            menu_availableSerialPorts["values"] = self.muxSystem.availableSerialports[2]

        def updatePixelSelection(self):
            #based on drop down menu, sends command to arduino to change selection
            self.muxSystem.selectedMode = menu_selectMode.get()
            self.muxSystem.selectedPixel = menu_selectPixel.get()
            self.muxSystem.command.commandString = self.muxSystem.command.brackets[self.muxSystem.command.commandBrackets][0] + 'pxl_' + str(self.muxSystem.selectedPixel) + '_' + str(self.muxSystem.selectedMode) + self.muxSystem.command.brackets[self.muxSystem.command.commandBrackets][1]
            self.muxSystem.arduinos[0].send(self.muxSystem.command.commandString)
            lbl_currentPixel['text'] = 'Selected Pixel: ' + self.muxSystem.selectedPixel
            lbl_currentMode['text'] = 'Selected Mode: ' + self.muxSystem.selectedMode + ' wire'
            tempReceived = tempMuxSystem.arduinos[0].receive()
            if (self.printCorrespondenceToTerminal.get() == 1):
                print(tempReceived)
            else:
                pass

        def returnToIdleState(self):
            # Sends string of <pxl_00_00> to arduino - arduino program understands what this means
            lbl_currentPixel['text'] = 'Selected Pixel: idle'
            lbl_currentMode['text'] = 'Selected Mode: idle'
            tempMuxSystem.command.commandString = '<pxl_00_00>'
            tempMuxSystem.arduinos[0].send(tempMuxSystem.command.commandString)
            tempReceived = tempMuxSystem.arduinos[0].receive()
            if(self.printCorrespondenceToTerminal.get() == 1):
                print(tempReceived)
            else:
                pass

        def panic(self):
            # sends emergency off, arduino writes LOW to all pins and exits its waiting loop. Requires restart of arduino before it will work again.
            #sends command !pxl_00_00!
            tempMuxSystem.command.commandString = '!pxl_00_00!'
            tempMuxSystem.arduinos[0].send(tempMuxSystem.command.commandString)
            lbl_currentPixel['text'] = 'OFF'
            lbl_currentMode['text'] = 'OFF'
            messagebox.showinfo('Error' , 'Emergency Off button pressed. Reset arduino before reselecting Serial Port in Setup Tab')
            tempReceived = tempMuxSystem.arduinos[0].receive()
            if (self.printCorrespondenceToTerminal.get() == 1):
                print(tempReceived)
            else:
                pass

        def yeet(self):
            #returns arduino to idle state, closes serial port.
            lbl_currentPixel['text'] = 'yeet'
            lbl_currentMode['text'] = 'yeet'
            self.pixelsMenu = [0]
            self.modeMenu = [0]
            tempMuxSystem.command.commandString = '<pxl_00_00>'
            tempMuxSystem.arduinos[0].send(tempMuxSystem.command.commandString)
            btn_updatePixelMode['state'] = tk.DISABLED
            btn_idleState['state'] = tk.DISABLED
            btn_PANIC['state'] = tk.DISABLED
            #btn_yeet['state'] = tk.DISABLED
            #btn_yolo['state'] = tk.DISABLED
            tempReceived = tempMuxSystem.arduinos[0].receive()
            self.pixelsMenu = [0]
            self.modeMenu = [0]
            menu_selectPixel['values'] = self.pixelsMenu
            menu_selectMode['values'] = self.modeMenu
            self.muxSystem.arduinos[0].comms.close()
            if (self.printCorrespondenceToTerminal.get() == 1):
                print(tempReceived)
            else:
                pass
            print('yeet')

        def yolo(self):
            #Enables all pixels because yolo
            #send command <pxl_100_4>
            tempMuxSystem.command.commandString = '<pxl_100_4>'
            tempMuxSystem.arduinos[0].send(tempMuxSystem.command.commandString)
            lbl_currentPixel['text'] = '#yolo'
            lbl_currentMode['text'] = '#yolo'
            tempReceived = tempMuxSystem.arduinos[0].receiveAll()
            if (self.printCorrespondenceToTerminal.get() == 1):
                print(tempReceived)
            else:
                pass
            print('#yolo')


        ###--------Configure GUI

        #menuBar = Menu(self.master)  # define menu bar

        #file_menu = Menu(menuBar)  # add item to menu bar
        #file_menu.add_command(label="New Command")  # add item to drop down menu, then need to generate instance of a cascade

        #program_menu = Menu(menuBar)
        #program_menu.add_command(label="Information")
        #program_menu.add_command(label="Quit")
        #program_menu.add_command(label="Maintenance Login")

        #edit_menu = Menu(menuBar)
        #edit_menu.add_command(label="First label here")

        #menuBar.add_cascade(label="AZ_4wire_MUX", menu=program_menu)  # generate a dropdown menu from items defined before
        #menuBar.add_cascade(label="File", menu=file_menu)
        #menuBar.add_cascade(label="Edit", menu=edit_menu)



        #####--------------------- Setup Tab
        #lbl_XXX.grid(column = X , row = Y)
        btn_searchForSerialPorts = Button(self.tab_setup , text = 'Search for available Serial Ports', command=lambda:searchForAvailableSerial(self))
        btn_searchForSerialPorts.grid(row = 0 , column = 0)
        btn_useSelectedSerialPort = Button(self.tab_setup, text = 'Use Selected Serial Port', command = lambda:selectSerialPort(self))
        btn_useSelectedSerialPort.grid(column = 1, row = 0)
        lbl_arduinoSerialPort = Label(self.tab_setup , text = 'Serial Port')
        lbl_arduinoSerialPort.grid(column=0, row=1)
        menu_availableSerialPorts = Combobox(self.tab_setup, values = self.muxSystem.availableSerialports[2] , width = 30)
        menu_availableSerialPorts.grid(column = 1 , row = 1)
        checkBox_printToTerminal = Checkbutton(self.tab_setup , text = 'Print Correspondence to Terminal' , variable = self.printCorrespondenceToTerminal)
        checkBox_printToTerminal.grid(column = 0 , row = 2)

        selectedPixelVar = 'Selected Pixel: '
        selectedModeVar  = 'Selected Mode: '
        lbl_currentPixel = Label(self.tab_control, text = 'Selected Pixel: ')
        lbl_currentPixel.grid(column = 1, row = 0)
        lbl_currentMode = Label(self.tab_control , text = 'Selected Mode: ')
        lbl_currentMode.grid(column = 2 , row = 0)
        lbl_selectPixel = Label(self.tab_control , text = 'Select Pixel')
        lbl_selectPixel.grid(column = 0 , row = 1)
        menu_selectPixel = Combobox(self.tab_control , values = self.pixelsMenu , width = 10)
        menu_selectPixel.grid(column = 1, row = 1)
        lbl_selectMode = Label(self.tab_control , text = 'Select Mode')
        lbl_selectMode.grid(column = 0 , row = 2)
        menu_selectMode = Combobox(self.tab_control , values = self.modeMenu, width = 10)
        menu_selectMode.grid(column = 1 , row = 2)

        btn_updatePixelMode = Button(self.tab_control , text = 'UPDATE MUX' , command = lambda:updatePixelSelection(self))
        btn_updatePixelMode.grid(column = 1 , row = 3)
        btn_updatePixelMode['state'] = tk.DISABLED
        btn_idleState = Button(self.tab_control , text = 'Idle State' , command = lambda:returnToIdleState(self))
        btn_idleState.grid(column = 3 , row = 1)
        btn_idleState['state'] = tk.DISABLED
        btn_PANIC = Button(self.tab_control, text='! PANIC ¡', command=lambda: panic(self))
        btn_PANIC.grid(column = 3 , row = 2)
        btn_PANIC['state'] = tk.DISABLED
        #btn_yeet = Button(self.tab_control , text = 'YEET' , command = lambda:yeet(self))
        #btn_yeet.grid(column = 3 , row = 3)
        #btn_yeet['state'] = tk.DISABLED
        #btn_yolo = Button(self.tab_control , text = '#yolo' , command = lambda:yolo(self))
        #btn_yolo.grid(column = 2 , row = 3)
        #btn_yolo['state'] = tk.DISABLED

        #self.master.config(menu=menuBar)



    def configureGUI(self):
        pass
    def runProgram(self):
        self.master.mainloop()



def main():
    systemConfig = muxSystem()
    systemConfig.availableSerialports = checkAvailableSerial()


    app = GUI_window(systemConfig)
    app.configureGUI()
    app.runProgram()




main()
