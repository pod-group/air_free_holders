EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 4wireMux_rev1-rescue:ASSR-1611-001E-Relay_SolidState K1
U 1 1 60339234
P 2050 3200
F 0 "K1" H 2050 3525 50  0000 C CNN
F 1 "ASSR-1611-001E" H 2050 3434 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 2800 3300 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 2800 2400 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 2800 3100 50  0001 L CNN "Description"
F 5 "4.7" H 2800 3000 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 2800 2900 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 2800 2500 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 2800 2700 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 2800 2600 50  0001 L CNN "Manufacturer_Part_Number"
	1    2050 3200
	1    0    0    -1  
$EndComp
$Comp
L 4wireMux_rev1-rescue:ASSR-1611-001E-Relay_SolidState K2
U 1 1 6033DEDA
P 2050 3850
F 0 "K2" H 2050 4175 50  0000 C CNN
F 1 "ASSR-1611-001E" H 2050 4084 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 2800 3950 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 2800 3050 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 2800 3750 50  0001 L CNN "Description"
F 5 "4.7" H 2800 3650 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 2800 3550 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 2800 3150 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 2800 3350 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 2800 3250 50  0001 L CNN "Manufacturer_Part_Number"
	1    2050 3850
	1    0    0    -1  
$EndComp
Text GLabel 1750 3200 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 2350 3100 2    50   Input ~ 0
SNS_Common
Text GLabel 2350 3750 2    50   Input ~ 0
SRC_Common
$Comp
L 4wireMux_rev1-rescue:ASSR-1611-001E-Relay_SolidState K3
U 1 1 6034AF0B
P 2050 4550
F 0 "K3" H 2050 4875 50  0000 C CNN
F 1 "ASSR-1611-001E" H 2050 4784 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 2800 4650 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 2800 3750 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 2800 4450 50  0001 L CNN "Description"
F 5 "4.7" H 2800 4350 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 2800 4250 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 2800 3850 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 2800 4050 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 2800 3950 50  0001 L CNN "Manufacturer_Part_Number"
	1    2050 4550
	1    0    0    -1  
$EndComp
$Comp
L 4wireMux_rev1-rescue:ASSR-1611-001E-Relay_SolidState K4
U 1 1 6034AF17
P 2050 5200
F 0 "K4" H 2050 5525 50  0000 C CNN
F 1 "ASSR-1611-001E" H 2050 5434 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 2800 5300 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 2800 4400 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 2800 5100 50  0001 L CNN "Description"
F 5 "4.7" H 2800 5000 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 2800 4900 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 2800 4500 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 2800 4700 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 2800 4600 50  0001 L CNN "Manufacturer_Part_Number"
	1    2050 5200
	1    0    0    -1  
$EndComp
Text GLabel 2350 4450 2    50   Input ~ 0
SNS_Common
Text GLabel 2350 5100 2    50   Input ~ 0
SRC_Common
$Comp
L 4wireMux_rev1-rescue:ASSR-1611-001E-Relay_SolidState K5
U 1 1 6034E1A5
P 2050 5900
F 0 "K5" H 2050 6225 50  0000 C CNN
F 1 "ASSR-1611-001E" H 2050 6134 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 2800 6000 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 2800 5100 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 2800 5800 50  0001 L CNN "Description"
F 5 "4.7" H 2800 5700 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 2800 5600 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 2800 5200 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 2800 5400 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 2800 5300 50  0001 L CNN "Manufacturer_Part_Number"
	1    2050 5900
	1    0    0    -1  
$EndComp
$Comp
L 4wireMux_rev1-rescue:ASSR-1611-001E-Relay_SolidState K6
U 1 1 6034E1B1
P 2050 6550
F 0 "K6" H 2050 6875 50  0000 C CNN
F 1 "ASSR-1611-001E" H 2050 6784 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 2800 6650 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 2800 5750 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 2800 6450 50  0001 L CNN "Description"
F 5 "4.7" H 2800 6350 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 2800 6250 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 2800 5850 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 2800 6050 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 2800 5950 50  0001 L CNN "Manufacturer_Part_Number"
	1    2050 6550
	1    0    0    -1  
$EndComp
Text GLabel 2350 5800 2    50   Input ~ 0
SNS_Common
Text GLabel 2350 6450 2    50   Input ~ 0
SRC_Common
$Comp
L 4wireMux_rev1-rescue:ASSR-1611-001E-Relay_SolidState K7
U 1 1 6035CE0D
P 4650 3200
F 0 "K7" H 4650 3525 50  0000 C CNN
F 1 "ASSR-1611-001E" H 4650 3434 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 5400 3300 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 5400 2400 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 5400 3100 50  0001 L CNN "Description"
F 5 "4.7" H 5400 3000 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 5400 2900 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 5400 2500 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 5400 2700 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 5400 2600 50  0001 L CNN "Manufacturer_Part_Number"
	1    4650 3200
	1    0    0    -1  
$EndComp
$Comp
L 4wireMux_rev1-rescue:ASSR-1611-001E-Relay_SolidState K8
U 1 1 6035CE19
P 4650 3850
F 0 "K8" H 4650 4175 50  0000 C CNN
F 1 "ASSR-1611-001E" H 4650 4084 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 5400 3950 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 5400 3050 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 5400 3750 50  0001 L CNN "Description"
F 5 "4.7" H 5400 3650 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 5400 3550 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 5400 3150 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 5400 3350 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 5400 3250 50  0001 L CNN "Manufacturer_Part_Number"
	1    4650 3850
	1    0    0    -1  
$EndComp
Text GLabel 4950 3100 2    50   Input ~ 0
SNS_Common
Text GLabel 4950 3750 2    50   Input ~ 0
SRC_Common
$Comp
L 4wireMux_rev1-rescue:ASSR-1611-001E-Relay_SolidState K9
U 1 1 6035CE29
P 4650 4550
F 0 "K9" H 4650 4875 50  0000 C CNN
F 1 "ASSR-1611-001E" H 4650 4784 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 5400 4650 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 5400 3750 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 5400 4450 50  0001 L CNN "Description"
F 5 "4.7" H 5400 4350 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 5400 4250 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 5400 3850 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 5400 4050 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 5400 3950 50  0001 L CNN "Manufacturer_Part_Number"
	1    4650 4550
	1    0    0    -1  
$EndComp
$Comp
L 4wireMux_rev1-rescue:ASSR-1611-001E-Relay_SolidState K10
U 1 1 6035CE35
P 4650 5200
F 0 "K10" H 4650 5525 50  0000 C CNN
F 1 "ASSR-1611-001E" H 4650 5434 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 5400 5300 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 5400 4400 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 5400 5100 50  0001 L CNN "Description"
F 5 "4.7" H 5400 5000 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 5400 4900 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 5400 4500 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 5400 4700 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 5400 4600 50  0001 L CNN "Manufacturer_Part_Number"
	1    4650 5200
	1    0    0    -1  
$EndComp
Text GLabel 4950 4450 2    50   Input ~ 0
SNS_Common
Text GLabel 4950 5100 2    50   Input ~ 0
SRC_Common
$Comp
L 4wireMux_rev1-rescue:ASSR-1611-001E-Relay_SolidState K11
U 1 1 6035CE45
P 4650 5900
F 0 "K11" H 4650 6225 50  0000 C CNN
F 1 "ASSR-1611-001E" H 4650 6134 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 5400 6000 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 5400 5100 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 5400 5800 50  0001 L CNN "Description"
F 5 "4.7" H 5400 5700 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 5400 5600 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 5400 5200 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 5400 5400 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 5400 5300 50  0001 L CNN "Manufacturer_Part_Number"
	1    4650 5900
	1    0    0    -1  
$EndComp
$Comp
L 4wireMux_rev1-rescue:ASSR-1611-001E-Relay_SolidState K12
U 1 1 6035CE51
P 4650 6550
F 0 "K12" H 4650 6875 50  0000 C CNN
F 1 "ASSR-1611-001E" H 4650 6784 50  0000 C CNN
F 2 "myParts:ASSR-1611-100E" H 5400 6650 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 5400 5750 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 5400 6450 50  0001 L CNN "Description"
F 5 "4.7" H 5400 6350 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 5400 6250 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 5400 5850 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 5400 6050 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 5400 5950 50  0001 L CNN "Manufacturer_Part_Number"
	1    4650 6550
	1    0    0    -1  
$EndComp
Text GLabel 4950 5800 2    50   Input ~ 0
SNS_Common
Text GLabel 4950 6450 2    50   Input ~ 0
SRC_Common
$Comp
L Connector:Conn_Coaxial J4
U 1 1 6035E15B
P 7200 3300
F 0 "J4" H 7300 3275 50  0000 L CNN
F 1 "Conn_Coaxial" H 7300 3184 50  0000 L CNN
F 2 "myParts:BNC_RA_radiall_R141665200" H 7200 3300 50  0001 C CNN
F 3 " ~" H 7200 3300 50  0001 C CNN
	1    7200 3300
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J5
U 1 1 6035F9E5
P 7200 4200
F 0 "J5" H 7300 4175 50  0000 L CNN
F 1 "Conn_Coaxial" H 7300 4084 50  0000 L CNN
F 2 "myParts:BNC_RA_radiall_R141665200" H 7200 4200 50  0001 C CNN
F 3 " ~" H 7200 4200 50  0001 C CNN
	1    7200 4200
	1    0    0    -1  
$EndComp
Text GLabel 7000 3300 0    50   Input ~ 0
SNS_Common
Text GLabel 7000 4200 0    50   Input ~ 0
SRC_Common
Text GLabel 7200 3500 3    50   Input ~ 0
SNS_GND
Text GLabel 7200 4400 3    50   Input ~ 0
SRC_GND
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J1
U 1 1 60364B10
P 2450 1650
F 0 "J1" H 2500 1125 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 2500 1216 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x06_Pitch2.54mm" H 2450 1650 50  0001 C CNN
F 3 "~" H 2450 1650 50  0001 C CNN
	1    2450 1650
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J2
U 1 1 60367005
P 3800 1650
F 0 "J2" H 3850 1125 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 3850 1216 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x06_Pitch2.54mm" H 3800 1650 50  0001 C CNN
F 3 "~" H 3800 1650 50  0001 C CNN
	1    3800 1650
	-1   0    0    1   
$EndComp
Text GLabel 4000 1350 2    50   Input ~ 0
pixel_6_SNS
Text GLabel 4000 1450 2    50   Input ~ 0
pixel_6_SRC
Text GLabel 4000 1550 2    50   Input ~ 0
pixel_4_SNS
Text GLabel 4000 1650 2    50   Input ~ 0
pixel_4_SRC
Text GLabel 4000 1750 2    50   Input ~ 0
pixel_2_SNS
Text GLabel 4000 1850 2    50   Input ~ 0
pixel_2_SRC
Text GLabel 2150 1350 0    50   Input ~ 0
pixel_5_SNS
Text GLabel 2150 1450 0    50   Input ~ 0
pixel_5_SRC
Text GLabel 2150 1550 0    50   Input ~ 0
pixel_3_SNS
Text GLabel 2150 1650 0    50   Input ~ 0
pixel_3_SRC
Text GLabel 2150 1850 0    50   Input ~ 0
pixel_1_SRC
Text GLabel 2150 1750 0    50   Input ~ 0
pixel_1_SNS
Text GLabel 2650 1550 2    50   Input ~ 0
TCO_SRC
Text GLabel 2650 1650 2    50   Input ~ 0
TCO_SNS
Text GLabel 3500 1550 0    50   Input ~ 0
TCO_SNS
Text GLabel 3500 1650 0    50   Input ~ 0
TCO_SRC
NoConn ~ 2650 1350
NoConn ~ 2650 1450
NoConn ~ 3500 1350
NoConn ~ 3500 1450
NoConn ~ 2650 1750
NoConn ~ 2650 1850
NoConn ~ 3500 1850
NoConn ~ 3500 1750
Text GLabel 8500 3500 0    50   Input ~ 0
SNS_GND
Text GLabel 8500 3700 0    50   Input ~ 0
SRC_GND
Text GLabel 8550 3500 2    50   Input ~ 0
TCO_SNS
Text GLabel 8550 3700 2    50   Input ~ 0
TCO_SRC
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 603735C1
P 1800 7850
F 0 "H1" H 1900 7899 50  0000 L CNN
F 1 "MountingHole_Pad" H 1900 7808 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 1800 7850 50  0001 C CNN
F 3 "~" H 1800 7850 50  0001 C CNN
	1    1800 7850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 603738F1
P 1800 8250
F 0 "H2" H 1900 8299 50  0000 L CNN
F 1 "MountingHole_Pad" H 1900 8208 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 1800 8250 50  0001 C CNN
F 3 "~" H 1800 8250 50  0001 C CNN
	1    1800 8250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 60374149
P 1800 8650
F 0 "H3" H 1900 8699 50  0000 L CNN
F 1 "MountingHole_Pad" H 1900 8608 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 1800 8650 50  0001 C CNN
F 3 "~" H 1800 8650 50  0001 C CNN
	1    1800 8650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 60374FE6
P 1800 9050
F 0 "H4" H 1900 9099 50  0000 L CNN
F 1 "MountingHole_Pad" H 1900 9008 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 1800 9050 50  0001 C CNN
F 3 "~" H 1800 9050 50  0001 C CNN
	1    1800 9050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H9
U 1 1 60379A4D
P 3200 7850
F 0 "H9" H 3300 7899 50  0000 L CNN
F 1 "MountingHole_Pad" H 3300 7808 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 3200 7850 50  0001 C CNN
F 3 "~" H 3200 7850 50  0001 C CNN
	1    3200 7850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H10
U 1 1 60379A53
P 3200 8250
F 0 "H10" H 3300 8299 50  0000 L CNN
F 1 "MountingHole_Pad" H 3300 8208 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 3200 8250 50  0001 C CNN
F 3 "~" H 3200 8250 50  0001 C CNN
	1    3200 8250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H11
U 1 1 60379A59
P 3200 8650
F 0 "H11" H 3300 8699 50  0000 L CNN
F 1 "MountingHole_Pad" H 3300 8608 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 3200 8650 50  0001 C CNN
F 3 "~" H 3200 8650 50  0001 C CNN
	1    3200 8650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H12
U 1 1 60379A5F
P 3200 9050
F 0 "H12" H 3300 9099 50  0000 L CNN
F 1 "MountingHole_Pad" H 3300 9008 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 3200 9050 50  0001 C CNN
F 3 "~" H 3200 9050 50  0001 C CNN
	1    3200 9050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H5
U 1 1 6037BEB9
P 1800 9650
F 0 "H5" H 1900 9699 50  0000 L CNN
F 1 "MountingHole_Pad" H 1900 9608 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 1800 9650 50  0001 C CNN
F 3 "~" H 1800 9650 50  0001 C CNN
	1    1800 9650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H6
U 1 1 6037BEBF
P 1800 10050
F 0 "H6" H 1900 10099 50  0000 L CNN
F 1 "MountingHole_Pad" H 1900 10008 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 1800 10050 50  0001 C CNN
F 3 "~" H 1800 10050 50  0001 C CNN
	1    1800 10050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H7
U 1 1 6037BEC5
P 1800 10450
F 0 "H7" H 1900 10499 50  0000 L CNN
F 1 "MountingHole_Pad" H 1900 10408 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 1800 10450 50  0001 C CNN
F 3 "~" H 1800 10450 50  0001 C CNN
	1    1800 10450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H8
U 1 1 6037BECB
P 1800 10850
F 0 "H8" H 1900 10899 50  0000 L CNN
F 1 "MountingHole_Pad" H 1900 10808 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 1800 10850 50  0001 C CNN
F 3 "~" H 1800 10850 50  0001 C CNN
	1    1800 10850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H13
U 1 1 6037BED1
P 3200 9650
F 0 "H13" H 3300 9699 50  0000 L CNN
F 1 "MountingHole_Pad" H 3300 9608 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 3200 9650 50  0001 C CNN
F 3 "~" H 3200 9650 50  0001 C CNN
	1    3200 9650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H14
U 1 1 6037BED7
P 3200 10050
F 0 "H14" H 3300 10099 50  0000 L CNN
F 1 "MountingHole_Pad" H 3300 10008 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 3200 10050 50  0001 C CNN
F 3 "~" H 3200 10050 50  0001 C CNN
	1    3200 10050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H15
U 1 1 6037BEDD
P 3200 10450
F 0 "H15" H 3300 10499 50  0000 L CNN
F 1 "MountingHole_Pad" H 3300 10408 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 3200 10450 50  0001 C CNN
F 3 "~" H 3200 10450 50  0001 C CNN
	1    3200 10450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H16
U 1 1 6037BEE3
P 3200 10850
F 0 "H16" H 3300 10899 50  0000 L CNN
F 1 "MountingHole_Pad" H 3300 10808 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 3200 10850 50  0001 C CNN
F 3 "~" H 3200 10850 50  0001 C CNN
	1    3200 10850
	1    0    0    -1  
$EndComp
Text GLabel 1800 7950 0    50   Input ~ 0
Arduino_GND
Text GLabel 1800 8350 0    50   Input ~ 0
Arduino_GND
Text GLabel 1800 8750 0    50   Input ~ 0
Arduino_GND
Text GLabel 1800 9150 0    50   Input ~ 0
Arduino_GND
Text GLabel 3200 9150 0    50   Input ~ 0
Arduino_GND
Text GLabel 3200 8750 0    50   Input ~ 0
Arduino_GND
Text GLabel 3200 8350 0    50   Input ~ 0
Arduino_GND
Text GLabel 3200 7950 0    50   Input ~ 0
Arduino_GND
Text GLabel 1800 9750 0    50   Input ~ 0
Arduino_GND
Text GLabel 1800 10150 0    50   Input ~ 0
Arduino_GND
Text GLabel 1800 10550 0    50   Input ~ 0
Arduino_GND
Text GLabel 1800 10950 0    50   Input ~ 0
Arduino_GND
Text GLabel 3200 10950 0    50   Input ~ 0
Arduino_GND
Text GLabel 3200 10550 0    50   Input ~ 0
Arduino_GND
Text GLabel 3200 10150 0    50   Input ~ 0
Arduino_GND
Text GLabel 3200 9750 0    50   Input ~ 0
Arduino_GND
Text GLabel 2350 3300 2    50   Input ~ 0
pixel_5_SNS
Text GLabel 2350 3950 2    50   Input ~ 0
pixel_5_SRC
Text GLabel 2350 4650 2    50   Input ~ 0
pixel_3_SNS
Text GLabel 2350 6000 2    50   Input ~ 0
pixel_1_SNS
Text GLabel 2350 6650 2    50   Input ~ 0
pixel_1_SRC
Text GLabel 4950 6650 2    50   Input ~ 0
pixel_2_SRC
Text GLabel 4950 6000 2    50   Input ~ 0
pixel_2_SNS
Text GLabel 4950 4650 2    50   Input ~ 0
pixel_4_SNS
Text GLabel 4950 5300 2    50   Input ~ 0
pixel_4_SRC
Text GLabel 4950 3300 2    50   Input ~ 0
pixel_6_SNS
Text GLabel 4950 3950 2    50   Input ~ 0
pixel_6_SRC
Text GLabel 6400 2600 0    50   Input ~ 0
Arduino_GND
$Comp
L Connector_Generic:Conn_01x17 J6
U 1 1 6038D700
P 11850 2050
F 0 "J6" H 11930 2092 50  0000 L CNN
F 1 "Conn_01x17" H 11930 2001 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x17_Pitch2.54mm" H 11850 2050 50  0001 C CNN
F 3 "~" H 11850 2050 50  0001 C CNN
	1    11850 2050
	1    0    0    -1  
$EndComp
NoConn ~ 13050 1850
NoConn ~ 13050 1950
$Comp
L Connector_Generic:Conn_01x17 J7
U 1 1 6039030B
P 12850 2050
F 0 "J7" H 12768 3067 50  0000 C CNN
F 1 "Conn_01x17" H 12768 2976 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x17_Pitch2.54mm" H 12850 2050 50  0001 C CNN
F 3 "~" H 12850 2050 50  0001 C CNN
	1    12850 2050
	-1   0    0    -1  
$EndComp
Text GLabel 13050 2750 2    50   Input ~ 0
3V3
Text GLabel 11650 1750 0    50   Input ~ 0
Arduino_GND
Text GLabel 11650 1650 0    50   Input ~ 0
Arduino_reset
Text GLabel 13050 2650 2    50   Input ~ 0
AREF
Text GLabel 13050 2850 2    50   Input ~ 0
Arduino_D13
Text GLabel 11650 2750 0    50   Input ~ 0
Arduino_D11
Text GLabel 11650 2650 0    50   Input ~ 0
Arduino_D10
Text GLabel 11650 2550 0    50   Input ~ 0
Arduino_D9
Text GLabel 11650 2450 0    50   Input ~ 0
Arduino_D8
Text GLabel 11650 2350 0    50   Input ~ 0
Arduino_D7
Text GLabel 11650 2250 0    50   Input ~ 0
Arduino_D6
Text GLabel 11650 2150 0    50   Input ~ 0
Arduino_D5
Text GLabel 11650 2050 0    50   Input ~ 0
Arduino_D4
Text GLabel 11650 1250 0    50   Input ~ 0
Arduino_D16
Text GLabel 11650 1350 0    50   Input ~ 0
Arduino_D17
Text GLabel 11650 1550 0    50   Input ~ 0
Arduino_D0
Text GLabel 11650 1450 0    50   Input ~ 0
Arduino_D1
Text GLabel 11650 2850 0    50   Input ~ 0
Arduino_D12
Text GLabel 11650 1950 0    50   Input ~ 0
Arduino_D3
Text GLabel 11650 1850 0    50   Input ~ 0
Arduino_D2
Text GLabel 13050 1250 2    50   Input ~ 0
Arduino_D15
Text GLabel 13050 1350 2    50   Input ~ 0
Arduino_D14
Text GLabel 13050 1450 2    50   Input ~ 0
Arduino_V_in
Text GLabel 13050 1550 2    50   Input ~ 0
Arduino_GND
Text GLabel 13050 1650 2    50   Input ~ 0
Arduino_reset
Text GLabel 13050 1750 2    50   Input ~ 0
Arduino_5V
Text GLabel 13050 2050 2    50   Input ~ 0
Arduino_D23
Text GLabel 13050 2150 2    50   Input ~ 0
Arduino_D22
Text GLabel 13050 2250 2    50   Input ~ 0
Arduino_D21
Text GLabel 13050 2350 2    50   Input ~ 0
Arduino_D20
Text GLabel 13050 2450 2    50   Input ~ 0
Arduino_D19
Text GLabel 13050 2550 2    50   Input ~ 0
Arduino_D18
Text GLabel 1750 6450 0    50   Input ~ 0
Arduino_D0
Text GLabel 1750 5800 0    50   Input ~ 0
Arduino_D1
Text GLabel 1750 5100 0    50   Input ~ 0
Arduino_D2
Text GLabel 1750 4450 0    50   Input ~ 0
Arduino_D3
Text GLabel 1750 3750 0    50   Input ~ 0
Arduino_D4
Text GLabel 1750 3100 0    50   Input ~ 0
Arduino_D5
Text GLabel 4350 6450 0    50   Input ~ 0
Arduino_D6
Text GLabel 4350 5800 0    50   Input ~ 0
Arduino_D7
Text GLabel 4350 5100 0    50   Input ~ 0
Arduino_D8
Text GLabel 4350 4450 0    50   Input ~ 0
Arduino_D9
Text GLabel 4350 3750 0    50   Input ~ 0
Arduino_D10
Text GLabel 4350 3100 0    50   Input ~ 0
Arduino_D11
Text GLabel 1750 3850 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 1750 4550 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 1750 5200 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 1750 5900 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 1750 6550 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 4350 6550 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 4350 5900 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 4350 5200 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 4350 4550 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 4350 3850 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 4350 3200 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 14350 2850 2    50   Input ~ 0
Arduino_D13
Text GLabel 14350 1250 2    50   Input ~ 0
Arduino_D15
Text GLabel 14350 1350 2    50   Input ~ 0
Arduino_D14
Text GLabel 14350 1450 2    50   Input ~ 0
Arduino_V_in
Text GLabel 14350 1550 2    50   Input ~ 0
Arduino_GND
Text GLabel 14350 1650 2    50   Input ~ 0
Arduino_reset
Text GLabel 14350 1750 2    50   Input ~ 0
Arduino_5V
Text GLabel 14350 2050 2    50   Input ~ 0
Arduino_D23
Text GLabel 14350 2150 2    50   Input ~ 0
Arduino_D22
Text GLabel 14350 2250 2    50   Input ~ 0
Arduino_D21
Text GLabel 14350 2350 2    50   Input ~ 0
Arduino_D20
Text GLabel 14350 2450 2    50   Input ~ 0
Arduino_D19
Text GLabel 14350 2550 2    50   Input ~ 0
Arduino_D18
NoConn ~ 14350 2850
NoConn ~ 14350 2550
NoConn ~ 14350 2450
NoConn ~ 14350 2350
NoConn ~ 14350 2150
NoConn ~ 14350 2250
NoConn ~ 14350 2050
NoConn ~ 14350 1750
NoConn ~ 14350 1650
NoConn ~ 14350 1450
NoConn ~ 14350 1350
NoConn ~ 14350 1250
Text GLabel 10650 1250 0    50   Input ~ 0
Arduino_D16
Text GLabel 10650 1350 0    50   Input ~ 0
Arduino_D17
Text GLabel 10650 2850 0    50   Input ~ 0
Arduino_D12
NoConn ~ 10650 2850
NoConn ~ 10650 1250
NoConn ~ 10650 1350
Text GLabel 6700 2400 2    50   Input ~ 0
pixel_1_SNS
Text GLabel 6700 2200 2    50   Input ~ 0
pixel_2_SNS
Text GLabel 6700 2000 2    50   Input ~ 0
pixel_3_SNS
Text GLabel 6700 1800 2    50   Input ~ 0
pixel_4_SNS
Text GLabel 6700 1600 2    50   Input ~ 0
pixel_5_SNS
Text GLabel 6700 1400 2    50   Input ~ 0
pixel_6_SNS
Text GLabel 6700 2300 2    50   Input ~ 0
TCO_SNS
Text GLabel 6700 1000 2    50   Input ~ 0
TCO_SRC
Text GLabel 6700 1100 2    50   Input ~ 0
pixel_6_SRC
Text GLabel 6700 1300 2    50   Input ~ 0
pixel_5_SRC
Text GLabel 6700 1500 2    50   Input ~ 0
pixel_4_SRC
Text GLabel 6700 1700 2    50   Input ~ 0
pixel_3_SRC
Text GLabel 6700 1900 2    50   Input ~ 0
pixel_2_SRC
Text GLabel 6700 2100 2    50   Input ~ 0
pixel_1_SRC
NoConn ~ 6700 1200
Wire Wire Line
	8500 3500 8550 3500
Wire Wire Line
	8500 3700 8550 3700
Text GLabel 2350 5300 2    50   Input ~ 0
pixel_3_SRC
Text GLabel 10750 3550 0    50   Input ~ 0
SSR_currLimResistor
Text GLabel 11050 3550 2    50   Input ~ 0
Arduino_GND
NoConn ~ 2350 3200
NoConn ~ 4950 3200
NoConn ~ 4950 3850
NoConn ~ 4950 4550
NoConn ~ 4950 5200
NoConn ~ 4950 5900
NoConn ~ 4950 6550
NoConn ~ 2350 6550
NoConn ~ 2350 5900
NoConn ~ 2350 5200
NoConn ~ 2350 4550
NoConn ~ 2350 3850
$Comp
L Device:R R1
U 1 1 61314599
P 10900 3550
F 0 "R1" V 10693 3550 50  0000 C CNN
F 1 "470R" V 10784 3550 50  0000 C CNN
F 2 "Resistor_SMD:R_2010_5025Metric_Pad1.52x2.65mm_HandSolder" V 10830 3550 50  0001 C CNN
F 3 "~" H 10900 3550 50  0001 C CNN
	1    10900 3550
	0    1    1    0   
$EndComp
$Comp
L Connector:DB15_Male_MountingHoles J3
U 1 1 6147F385
P 6400 1700
F 0 "J3" H 6306 2692 50  0000 C CNN
F 1 "DB15_Male_MountingHoles" H 6306 2601 50  0000 C CNN
F 2 "Connector_Dsub:DSUB-15_Male_Horizontal_P2.77x2.84mm_EdgePinOffset4.94mm_Housed_MountingHolesOffset7.48mm" H 6400 1700 50  0001 C CNN
F 3 " ~" H 6400 1700 50  0001 C CNN
	1    6400 1700
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J8
U 1 1 614DDC85
P 12850 4300
F 0 "J8" H 12768 3975 50  0000 C CNN
F 1 "Conn_01x02" H 12768 4066 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 12850 4300 50  0001 C CNN
F 3 "~" H 12850 4300 50  0001 C CNN
	1    12850 4300
	-1   0    0    1   
$EndComp
Text GLabel 13050 4200 2    50   Input ~ 0
Arduino_reset
Text GLabel 13050 4300 2    50   Input ~ 0
Arduino_GND
$EndSCHEMATC
